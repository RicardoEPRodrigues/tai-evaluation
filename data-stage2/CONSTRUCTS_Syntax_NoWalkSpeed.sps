﻿* Encoding: UTF-8.

*IMPORTANT: Change path to your project dir.
CD "/home/ricardo/Documents/Projects/tai-evaluation/data-stage2/".

* Load file for Elbow/Silhouette Methods.
INCLUDE "KO_Internal_clustering_criteria_EN/Internal clustering criteria.sps".

GET FILE='Ricardo+-+TAI+Method+-+Adfectus+-+Part+2_May+5,+2023_17.58_CONSTRUCTS.sav'.
DATASET NAME DataSet1 WINDOW=FRONT.
DATASET ACTIVATE DataSet1.

DELETE VARIABLES
    FAC1_1
    FAC2_1
    FAC3_1
    FAC4_1
    FAC5_1
    KMean_2C
    KMean_3C
    KMean_4C
    KMean_5C
    KMean_6C
    KMean_7C
    KMean_8C
    KMean_9C
    KMean_10C
    n_KMean_2C
    s_KMean_2C
    n_KMean_3C
    s_KMean_3C
    n_KMean_4C
    s_KMean_4C
    n_KMean_5C
    s_KMean_5C
    n_KMean_6C
    s_KMean_6C
    n_KMean_7C
    s_KMean_7C
    n_KMean_8C
    s_KMean_8C
    n_KMean_9C
    s_KMean_9C
    n_KMean_10C
    s_KMean_10C
    FINAL_KMean_CL
    FINAL_KMean_Dist.

*AVERAGE AND STD Deviation.
DESCRIPTIVES VARIABLES=GroundedPosture.ShakyPosture NaturalExpression.UnnaturalExpression
    WeaponHeldHigh.WeaponHeldLow GrimaceFacialExpression.CheerfulFacialExpression ReadytoReact.Relaxed
    Prepared.Unprepared FastWalkSpeed.SlowWalkSpeed WeaponHeldinFront.WeaponHeldBack
    LeaningBackUpright.LeaningForward CloseBodyPosture.OpenBodyPosture HeadDownLow.HeadUpHigh
    OffensiveStance.DefensiveStance
    /STATISTICS=MEAN STDDEV MIN MAX.


* PCA.
FACTOR
    /VARIABLES GroundedPosture.ShakyPosture WeaponHeldHigh.WeaponHeldLow
    GrimaceFacialExpression.CheerfulFacialExpression ReadytoReact.Relaxed Prepared.Unprepared
    WeaponHeldinFront.WeaponHeldBack LeaningBackUpright.LeaningForward
    CloseBodyPosture.OpenBodyPosture HeadDownLow.HeadUpHigh OffensiveStance.DefensiveStance
    /MISSING LISTWISE
    /ANALYSIS GroundedPosture.ShakyPosture WeaponHeldHigh.WeaponHeldLow
    GrimaceFacialExpression.CheerfulFacialExpression ReadytoReact.Relaxed Prepared.Unprepared
    WeaponHeldinFront.WeaponHeldBack LeaningBackUpright.LeaningForward
    CloseBodyPosture.OpenBodyPosture HeadDownLow.HeadUpHigh OffensiveStance.DefensiveStance
    /PRINT INITIAL CORRELATION EXTRACTION ROTATION
    /FORMAT SORT BLANK(.4)
    /PLOT EIGEN
    /CRITERIA FACTORS(5) ITERATE(25)
    /EXTRACTION PC
    /CRITERIA ITERATE(25)
    /ROTATION VARIMAX
    /SAVE REG(ALL)
    /METHOD=CORRELATION.

DEFINE !rename_PCA_labels ()
    !LET !NUM_OF_COMP = 5
    !LET !MINUS_ONE = 1
    !DO !i=1 !TO !NUM_OF_COMP
        !LET !NAME=!CONCAT("FAC", !i, "_1") .
        VARIABLE LABELS !NAME !CONCAT("PCA Component #", !I) .
    !DOEND
!ENDDEFINE.

!rename_PCA_labels.

* Chart of Clustered Means of the PCA Components by Emotions.
GGRAPH
  /GRAPHDATASET NAME="graphdataset" VARIABLES=Emotion MEANCI(FAC1_1, 95) MEANCI(FAC2_1, 95) 
    MEANCI(FAC3_1, 95) MEANCI(FAC4_1, 95) MEANCI(FAC5_1, 95) MISSING=LISTWISE REPORTMISSING=NO
    TRANSFORM=VARSTOCASES(SUMMARY="#SUMMARY" INDEX="#INDEX" LOW="#LOW" HIGH="#HIGH")
  /GRAPHSPEC SOURCE=INLINE
  /FRAME OUTER=NO INNER=NO
  /GRIDLINES XAXIS=NO YAXIS=YES.
BEGIN GPL
  SOURCE: s=userSource(id("graphdataset"))
  DATA: Emotion=col(source(s), name("Emotion"), unit.category())
  DATA: SUMMARY=col(source(s), name("#SUMMARY"))
  DATA: INDEX=col(source(s), name("#INDEX"), unit.category())
  DATA: LOW=col(source(s), name("#LOW"))
  DATA: HIGH=col(source(s), name("#HIGH"))
  COORD: rect(dim(1,2), cluster(3,0))
  GUIDE: axis(dim(3), label("Emotion"))
  GUIDE: axis(dim(2), label("Mean"))
  GUIDE: legend(aesthetic(aesthetic.color.interior), label(""))
  GUIDE: text.title(label("Clustered Means of the PCA Components by Emotions"))
  GUIDE: text.footnote(label("Error Bars: 95% CI"))
  SCALE: cat(dim(3), include("confident", "defensive", "fear", "rage"))
  SCALE: linear(dim(2), include(0))
  SCALE: cat(aesthetic(aesthetic.color.interior), include("0", "1", "2", "3", "4"))
  SCALE: cat(dim(1), include("0", "1", "2", "3", "4"))
  ELEMENT: point(position(INDEX*SUMMARY*Emotion), color.interior(INDEX))
  ELEMENT: interval(position(region.spread.range(INDEX*(LOW+HIGH)*Emotion)), 
    shape.interior(shape.ibeam), color.interior(INDEX))
END GPL.


* IMPORTANT: Here we are trying to find the best number of clusters to use for the K-Means Cluster Analysis
    *
    * One way is to use the Hierarchical Cluster Analysis and looking at which distance we can make a stable split, see comments bellow
    *
    * Another way is to see the Elbow Method and the Silhouette Method and determine the number of clusters based on the merge of both, see code bellow
    * This is the method we use.

*Hierarchical Cluster Analysis to help determine the number of clusters for the K-Means Cluster Analysis.
* CLUSTER   FAC1_1 FAC2_1 FAC3_1 FAC4_1 FAC5_1
    /METHOD WARD
    /MEASURE=SEUCLID
    /PRINT SCHEDULE
    /PLOT DENDROGRAM VICICLE.
*Looking at the Dendrogram, with a distance of 15 a stable split between clusters is seen and results in 5 clusters

* We run and store KMeans Cluster Analysis with different cluster numbering.
DEFINE !cluster_loop ()
    !DO !i=2 !TO 10
        !LET !NAME=!CONCAT("KMean_", !I, "C") .
        QUICK CLUSTER FAC1_1 FAC2_1 FAC3_1 FAC4_1 FAC5_1
        /MISSING=LISTWISE
        /CRITERIA=CLUSTER(!i) MXITER(99) CONVERGE(0)
        /METHOD=KMEANS(NOUPDATE)
        /SAVE CLUSTER(!NAME)
        /PRINT INITIAL ANOVA.
        VARIABLE LABELS !NAME !CONCAT("KMean With ", !I, " Clusters") .
    !DOEND
!ENDDEFINE.

!cluster_loop.

* Compute Elbow Method in the SSWithin graph.
CD "/tmp/"
    !KO_calharv  vars= FAC1_1 FAC2_1 FAC3_1 FAC4_1 FAC5_1
    /clusol= KMean_2C
                    KMean_3C
                    KMean_4C
                    KMean_5C
                    KMean_6C
                    KMean_7C
                    KMean_8C
                    KMean_9C
                    KMean_10C.

DATASET ACTIVATE DataSet1.

* Compute Silhouette Method.
!KO_sildev vars= FAC1_1 FAC2_1 FAC3_1 FAC4_1 FAC5_1
    /clusol= KMean_2C
                    KMean_3C
                    KMean_4C
                    KMean_5C
                    KMean_6C
                    KMean_7C
                    KMean_8C
                    KMean_9C
                    KMean_10C
    /caps= 'n_' 's_'
    /single= 0.

DATASET ACTIVATE DataSet1.

* The Elbow/Silhouette method points to 5 Clusters.
* The Calinski-Harabasz index also point to 5 Clusters. (https://towardsdatascience.com/calinski-harabasz-index-for-k-means-clustering-evaluation-using-python-4fefeeb2988e)
* 
* The current data points to the use of 5 Clusters. Let's compute the K-means on that.
QUICK CLUSTER FAC1_1 FAC2_1 FAC3_1 FAC4_1 FAC5_1
    /MISSING=LISTWISE
    /CRITERIA=CLUSTER(6) MXITER(99) CONVERGE(0)
    /METHOD=KMEANS(NOUPDATE)
    /SAVE CLUSTER(FINAL_KMean_CL) DISTANCE(FINAL_KMean_Dist)
    /PRINT INITIAL ANOVA.
VARIABLE LABELS FINAL_KMean_CL "Final KMean With 6 Clusters".
VARIABLE LABELS FINAL_KMean_Dist "Final Distance of each Case from its Classification Cluster Center".

* Print Table counting the number of occurences of emotions per cluster.
CTABLES
    /VLABELS VARIABLES=Emotion FINAL_KMean_CL DISPLAY=LABEL
    /TABLE Emotion [C][COUNT F40.0] BY FINAL_KMean_CL [C]
    /SLABELS VISIBLE=NO
    /CATEGORIES VARIABLES=Emotion ORDER=A KEY=VALUE EMPTY=INCLUDE TOTAL=YES POSITION=AFTER
    /CATEGORIES VARIABLES=FINAL_KMean_CL ORDER=A KEY=VALUE EMPTY=EXCLUDE
    /CRITERIA CILEVEL=95.


* Chart with the Count of Emotions in Each Cluster.
GGRAPH
  /GRAPHDATASET NAME="graphdataset" VARIABLES=FINAL_KMean_CL COUNT()[name="COUNT"] Emotion 
    MISSING=LISTWISE REPORTMISSING=NO
  /GRAPHSPEC SOURCE=INLINE
  /COLORCYCLE COLOR1(85,150,230), COLOR2(28,205,205), COLOR3(204,127,228), COLOR4(215,0,51), 
    COLOR5(227,215,16), COLOR6(0,180,160), COLOR7(255,196,226), COLOR8(171,73,243), COLOR9(95,195,56), 
    COLOR10(63,90,168), COLOR11(254,130,180), COLOR12(208,202,140), COLOR13(204,134,63), 
    COLOR14(119,55,143), COLOR15(236,230,208), COLOR16(69,70,71), COLOR17(92,202,136), 
    COLOR18(208,83,52), COLOR19(204,127,228), COLOR20(225,188,29), COLOR21(237,75,75), 
    COLOR22(28,205,205), COLOR23(92,113,72), COLOR24(225,139,14), COLOR25(9,38,114), 
    COLOR26(90,100,94), COLOR27(155,0,0), COLOR28(207,172,227), COLOR29(150,145,145), 
    COLOR30(63,235,124)
  /FRAME OUTER=NO INNER=NO
  /GRIDLINES XAXIS=NO YAXIS=YES.
BEGIN GPL
  SOURCE: s=userSource(id("graphdataset"))
  DATA: FINAL_KMean_CL=col(source(s), name("FINAL_KMean_CL"), unit.category())
  DATA: COUNT=col(source(s), name("COUNT"))
  DATA: Emotion=col(source(s), name("Emotion"), unit.category())
  GUIDE: axis(dim(1), label("Clusters"))
  GUIDE: axis(dim(2), label("Count"))
  GUIDE: legend(aesthetic(aesthetic.color.interior), label("Emotion"))
  GUIDE: text.title(label("Count of Emotions in Each Cluster"))
  SCALE: linear(dim(2), include(0))
  SCALE: cat(aesthetic(aesthetic.color.interior), include("confident", "defensive", "fear", "rage"))    
  ELEMENT: interval.stack(position(FINAL_KMean_CL*COUNT), color.interior(Emotion), 
    shape.interior(shape.square))
END GPL.
