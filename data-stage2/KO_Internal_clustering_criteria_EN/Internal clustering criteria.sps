﻿* Encoding: UTF-8.
*Internal clustering criteria
*SPSS macros by Kirill Orlov
*kior@comtv.ru
*https://www.spsstools.net/en/KO-spssmacros
*All rights reserved.

*/*!KO_CALHARV*/vers3***********************************************.
define !KO_calharv(vars= !charend('/') /missing= !charend('/') /clusol= !charend('/'))
echo ''.
echo '**** CALINSKI-HARABASZ, SSW, and LOG SS RATIO CLUSTERING CRITERIA (INPUT - VARIABLES) ****'.
set results none.
!if (!missing='VARIABLE') !then !let !miss= '-999' !else !let !miss= 'omit' !ifend
exec.
!do !clu !in (!clusol)
-temp.
-select if not miss(!clu) and !clu<>0.
-matrix.
-get vars /vari= !clu !vars /miss= !miss.
-comp ssw= -999.
-comp calhar= -999.
-comp lssrat= -999.
-do if cmin(vars(:,1))<>cmax(vars(:,1)).
- comp clu= design(vars(:,1)).
- comp vars= vars(:,2:ncol(vars)).
- comp valid= vars<>-999.
- comp vars= vars&*valid.
- comp n= t(valid)*clu.
- comp k= rsum(n>0).
- do if all(k>=2).
-  comp nt= rsum(n).
-  comp sum= t(vars)*clu.
-  comp ss= t(vars&*vars)*clu.
-  comp sum2= sum&*sum/(n+(n=0)).
-  comp ssw= rsum(ss-sum2).
-  comp ssb= rsum(sum2)-rsum(sum)&**2/nt.
-  comp calhar= csum(ssb&*(nt-k))/csum(ssw&*(k-1)).
-  comp ssb= csum(ssb).
-  comp ssw= csum (ssw).
-  do if ssb and ssw.
-   comp lssrat= ln(ssb/ssw).
-  end if.
- end if.
-end if.
-save {!quote(!clu),calhar,ssw,lssrat} /out= !quote(!conc('temp_KO_calharv_',!clu,'.sav')) /vari= clusol calhar ssw lssrat /strings= clusol.
-end matrix.
!doend
set results list.
add files /file= !quote(!conc('temp_KO_calharv_',!head(!clusol),'.sav')). 
!do !clu !in (!tail(!clusol))
-add files /file= * /file= !quote(!conc('temp_KO_calharv_',!clu,'.sav')).
!doend
recode calhar ssw lssrat (-999=sysmis).
cache.
exec.
var lab clusol 'Cluster solution' calhar 'Calinski-Harabasz' ssw 'Pooled SSwithin' lssrat 'Log SS ratio'.
var lev calhar ssw lssrat (sca).
format calhar ssw lssrat (f8.3).
list clusol calhar ssw lssrat.
!if (!tail(!clusol)<>!null) !then
-GRAPH /LINE(SIMPLE)= VALUE(calhar) BY clusol /TITLE= 'Comparison of cluster solutions (Calinski-Harabasz)' /FOOTNOTE= 'The higher the value the "better" is solution'.
-GRAPH /LINE(SIMPLE)= VALUE(ssw) BY clusol /TITLE= 'Comparison of cluster solutions (Pooled SSwithin)' /FOOTNOTE= 'See bend up ("bottom elbow")'.
-GRAPH /LINE(SIMPLE)= VALUE(lssrat) BY clusol /TITLE= 'Comparison of cluster solutions (Log SS ratio)' /FOOTNOTE= 'See bend down ("top elbow")'.
!ifend
!do !clu !in (!clusol)
-erase file !quote(!conc('temp_KO_calharv_',!clu,'.sav')).
!doend
!enddefine.

*/*!KO_CALHARM*/vers3***********************************************.
define !KO_calharm(matrix= !charend('/') /square= !charend('/') /clusol= !charend('/'))
echo ''.
echo '**** CALINSKI-HARABASZ, SSW, and LOG SS RATIO CLUSTERING CRITERIA (INPUT - MATRIX) ****'.
!let !matr= !matrix !let !fcrop= 0 !let !lcrop= 0
!if (!substr(!matr,1,1)='?') !then !let !matr= !substr(!matr,2) !let !fcrop= 1 !ifend
!let !lm= !len(!matr)
!if (!substr(!matr,!lm,1)='?') !then !let !matr= !substr(!conc(' ',!matr),1,!lm) !let !lcrop= 1 !ifend
exec.
comp casenum#= $casenum.
!do !clu !in (!clusol)
 !if (!clu=!head(!tail(!clusol))) !then
- set results none.
 !ifend
-temp.
-select if not miss(!clu) and !clu<>0.
-matrix.
-get d /vari= casenum# !matr.
 !if (!fcrop) !then
- comp d= d(:,{1,3:ncol(d)}).
 !ifend
 !if (!lcrop) !then
- comp d= d(:,1:(ncol(d)-1)).
 !ifend
-comp d= d(:,d(:,1)+1).
 !if (!clu=!head(!clusol)) !then
- do if d(1,1)<>0.
-  print /title '>ERROR. Diagonal elements in matrix are not zero. Demanded is distance matrix.'.
-  print /title '>Possibly, you input similarity matrix.' /space= 0.
-  print /title '>!KO_CALHARM will continue but the result is expected to be invalid.' /space= 0.
- end if.
 !ifend
 !if (!square='YES') !then
- comp d= d&*d.
 !ifend
-get clu /var= !clu.
-comp ssw= -999.
-comp calhar= -999.
-comp lssrat= -999.
-do if cmin(clu)<>cmax(clu).
- comp nt= nrow(clu).
- comp sst= msum(d)/(2*nt).
- comp clu= design(clu).
- comp n= csum(clu).
- comp ssw= csum(diag(t(clu)*d*clu)/t(2*n)).
- comp ssb= sst-ssw.
- comp calhar= ssb/ssw*(nt-ncol(clu))/(ncol(clu)-1).
- do if ssb and ssw.
-  comp lssrat= ln(ssb/ssw).
- end if.
-end if.
-save {!quote(!clu),calhar,ssw,lssrat} /out= !quote(!conc('temp_KO_calharm_',!clu,'.sav')) /vari= clusol calhar ssw lssrat /string= clusol.
-end matrix.
!doend
del var casenum#.
set results list.
add files /file= !quote(!conc('temp_KO_calharm_',!head(!clusol),'.sav')). 
!do !clu !in (!tail(!clusol))
-add files /file= * /file= !quote(!conc('temp_KO_calharm_',!clu,'.sav')).
!doend
recode calhar ssw lssrat (-999=sysmis).
cache.
exec.
var lab clusol 'Cluster solution' calhar 'Calinski-Harabasz' ssw 'Pooled SSwithin' lssrat 'Log SS ratio'.
var lev calhar ssw lssrat (sca).
format calhar ssw lssrat (f8.3).
list clusol calhar ssw lssrat.
!if (!tail(!clusol)<>!null) !then
-GRAPH /LINE(SIMPLE)= VALUE(calhar) BY clusol /TITLE= 'Comparison of cluster solutions (Calinski-Harabasz)' /FOOTNOTE= 'The higher the value the "better" is solution'.
-GRAPH /LINE(SIMPLE)= VALUE(ssw) BY clusol /TITLE= 'Comparison of cluster solutions (Pooled SSwithin)' /FOOTNOTE= 'See bend up ("bottom elbow")'.
-GRAPH /LINE(SIMPLE)= VALUE(lssrat) BY clusol /TITLE= 'Comparison of cluster solutions (Log SS ratio)' /FOOTNOTE= 'See bend down ("top elbow")'.
!ifend
!do !clu !in (!clusol)
-erase file !quote(!conc('temp_KO_calharm_',!clu,'.sav')).
!doend
!enddefine.

*/*!KO_DAVBOULV*/vers3***********************************************.
define !KO_davboulv(vars= !charend('/') /clusol= !charend('/') /norm= !charend('/'))
echo ''.
echo '**** DAVIES-BOULDIN, PBM CLUSTERING CRITERIA (INPUT - VARIABLES) ****'.
set mxloops 100000 results none.
exec.
!do !clu !in (!clusol)
-temp.
-select if not miss(!clu) and !clu<>0.
-matrix.
-get vars /vari= !clu !vars /miss= omit.
-do if cmin(vars(:,1))=cmax(vars(:,1)).
- comp davboul= -999.
- comp pbm= -999.
-else.
- comp clu= design(vars(:,1)).
- comp vars= vars(:,2:ncol(vars)).
- comp k= ncol(clu).
- comp n= csum(clu)(make(1,ncol(vars),1),:).
- comp sum= t(vars)*clu.
- comp mean= sum/n.
  !if (!norm='L1') !then !let !nrm= ', L1'
-  comp s= t(abs(vars-clu*t(mean)))*clu.
-  comp s= csum(s)/n(1,:).
-  comp et= msum(abs(vars-(csum(vars)/nrow(vars))(make(1,nrow(vars),1),:))).
-  comp ew= msum(abs(vars-clu*t(mean))).
-  release vars.
-  comp dc= make(k,k,0).
-  loop i= 1 to nrow(mean).
-   comp dci= mean(i,:)(make(1,k,1),:).
-   comp dc= dc+abs(dci-t(dci)).
-  end loop.
  !else
-  comp s= t(vars&*vars)*clu.
-  comp s= s-sum&*mean.
-  comp s= sqrt(csum(s)/n(1,:)).
-  comp et= rssq(vars-(csum(vars)/nrow(vars))(make(1,nrow(vars),1),:)).
-  comp et= csum(sqrt(et)).
-  comp ew= csum(sqrt(rssq(vars-clu*t(mean)))).
-  release vars.
-  comp dc= sscp(mean).
-  comp hsq= diag(dc)*make(1,ncol(dc),1).
-  comp dc= sqrt(hsq+t(hsq)-2*dc).
  !ifend
- comp maxdc= mmax(dc).
- call setdiag(dc,1).
- do if not all(dc).
-  comp dc= dc+.000000001.
- end if.
- comp s= s(make(1,k,1),:).
- comp s= (s+t(s))/dc.
- call setdiag(s,0).
- comp davboul= csum(rmax(s))/k.
- do if ew.
-  comp pbm= 1/k*et/ew*maxdc.
- else.
-  comp pbm= -999.
- end if.
-end if.
-save {!quote(!clu),davboul,pbm} /out= !quote(!conc('temp_KO_davboulv_',!clu,'.sav')) /vari= clusol davboul pbm /string= clusol.
-end matrix.
!doend
set results list.
add files /file= !quote(!conc('temp_KO_davboulv_',!head(!clusol),'.sav')). 
!do !clu !in (!tail(!clusol))
-add files /file= * /file= !quote(!conc('temp_KO_davboulv_',!clu,'.sav')).
!doend
recode davboul pbm (-999=sysmis).
cache.
exec.
var lab davboul !quote(!conc('Davies-Bouldin',!nrm)) pbm !quote(!conc('PBM',!nrm)) clusol 'Cluster solution'.
var lev davboul pbm (sca).
format davboul pbm (f8.3).
list clusol davboul pbm.
!if (!tail(!clusol)<>!null) !then
-GRAPH /LINE(SIMPLE)= VALUE(davboul) BY clusol /TITLE= !quote(!conc('Comparison of cluster solutions (Davies-Bouldin',!nrm,')'))
       /FOOTNOTE= 'The lower the value the "better" is solution'.
-GRAPH /LINE(SIMPLE)= VALUE(pbm) BY clusol /TITLE= !quote(!conc('Comparison of cluster solutions (PBM',!nrm,')'))
       /FOOTNOTE= 'The higher the value the "better" is solution'.
!ifend
!do !clu !in (!clusol)
-erase file !quote(!conc('temp_KO_davboulv_',!clu,'.sav')).
!doend
!enddefine.

*/*!KO_DAVBOULM*/vers2***********************************************.
define !KO_davboulm(matrix= !charend('/') /square= !charend('/') /clusol= !charend('/'))
echo ''.
echo '**** DAVIES-BOULDIN, PBM CLUSTERING CRITERIA (INPUT - MATRIX) ****'.
!let !matr= !matrix !let !fcrop= 0 !let !lcrop= 0
!if (!substr(!matr,1,1)='?') !then !let !matr= !substr(!matr,2) !let !fcrop= 1 !ifend
!let !lm= !len(!matr)
!if (!substr(!matr,!lm,1)='?') !then !let !matr= !substr(!conc(' ',!matr),1,!lm) !let !lcrop= 1 !ifend
exec.
comp casenum#= $casenum.
!do !clu !in (!clusol)
 !if (!clu=!head(!tail(!clusol))) !then
- set results none.
 !ifend
-temp.
-select if not miss(!clu) and !clu<>0.
-matrix.
-get d /vari= casenum# !matr.
 !if (!fcrop) !then
- comp d= d(:,{1,3:ncol(d)}).
 !ifend
 !if (!lcrop) !then
- comp d= d(:,1:(ncol(d)-1)).
 !ifend
-comp d= d(:,d(:,1)+1).
 !if (!clu=!head(!clusol)) !then
- do if d(1,1)<>0.
-  print /title '>ERROR. Diagonal elements in matrix are not zero. Demanded is distance matrix.'.
-  print /title '>Possibly, you input similarity matrix.' /space= 0.
-  print /title '>!KO_DAVBOULM will continue but the result is expected to be invalid.' /space= 0.
- end if.
 !ifend
 !if (!square='YES') !then
- comp d= d&*d.
 !ifend
-get clu /var= !clu.
-do if cmin(clu)=cmax(clu).
- comp davboul= -999.
- comp pbm= -999.
-else.
- comp clu= design(clu).
- comp k= ncol(clu).
- comp n= csum(clu).
- comp ones= make(1,nrow(clu),1).
- comp pgs= d*clu.
- comp bgs= t(clu)*pgs.
- comp wgs= diag(bgs)/2.
- comp nsq= n&**2.
- comp s= sqrt(t(wgs)/nsq).
- comp ew= (pgs-(t(wgs)/n)(ones,:))/n(ones,:).
- comp ew= msum(sqrt(ew&*clu)).
- comp rmean= (rsum(d)/ncol(d))*ones.
- comp d= (rmean+t(rmean)-d-msum(d)/ncol(d)**2)/2.
- comp et= csum(sqrt(diag(d))).
- release d.
- comp nn= sscp(n).
- comp dc= wgs*nsq.
- comp dc= (bgs-(dc+t(dc))/nn)/nn.
- call setdiag(dc,0).
- comp dc= sqrt(dc).
- comp maxdc= mmax(dc).
- call setdiag(dc,1).
- do if not all(dc).
-  comp dc= dc+.000000001.
- end if.
- comp s= s(make(1,k,1),:).
- comp s= (s+t(s))/dc.
- call setdiag(s,0).
- comp davboul= csum(rmax(s))/k.
- do if ew.
-  comp pbm= 1/k*et/ew*maxdc.
- else.
-  comp pbm= -999.
- end if.
-end if.
-save {!quote(!clu),davboul,pbm} /out= !quote(!conc('temp_KO_davboulm_',!clu,'.sav')) /vari= clusol davboul pbm /string= clusol.
-end matrix.
!doend
del var casenum#.
set results list.
add files /file= !quote(!conc('temp_KO_davboulm_',!head(!clusol),'.sav')). 
!do !clu !in (!tail(!clusol))
-add files /file= * /file= !quote(!conc('temp_KO_davboulm_',!clu,'.sav')).
!doend
recode davboul pbm (-999=sysmis).
cache.
exec.
var lab davboul 'Davies-Bouldin' pbm 'PBM' clusol 'Cluster solution'.
var lev davboul pbm (sca).
format davboul pbm (f8.3).
list clusol davboul pbm.
!if (!tail(!clusol)<>!null) !then
-GRAPH /LINE(SIMPLE)= VALUE(davboul) BY clusol /TITLE= 'Comparison of cluster solutions (Davies-Bouldin)' /FOOTNOTE= 'The lower the value the "better" is solution'.
-GRAPH /LINE(SIMPLE)= VALUE(pbm) BY clusol /TITLE= 'Comparison of cluster solutions (PBM)' /FOOTNOTE= 'The higher the value the "better" is solution'.
!ifend
!do !clu !in (!clusol)
-erase file !quote(!conc('temp_KO_davboulm_',!clu,'.sav')).
!doend
!enddefine.

*/*!KO_CCCRITV*/vers2***********************************************.
define !KO_cccritv(vars= !charend('/') /clusol= !charend('/') /fast= !charend('/'))
echo ''.
echo '**** CUBIC CLUSTERING CRITERION, LOG DET RATIO CLUSTERING CRITERION (INPUT - VARIABLES) ****'.
set mxloops 10000 results none.
!if (!fast='YES' !and !tail(!clusol)<>!null) !then !let !fst= 1 !else !let !fst= 0 !ifend
exec.
!do !clu !in (!clusol)
-temp.
-select if not miss(!clu) and !clu<>0.
-matrix.
-get vars /vari= !clu !vars /miss= omit.
-comp ccc= -999.
-comp ldetrat= -999.
-do if cmin(vars(:,1))<>cmax(vars(:,1)).
- comp clu= design(vars(:,1)).
- comp vars= vars(:,2:ncol(vars)).
- comp n= csum(clu).
- comp k= ncol(clu).
- comp nt= nrow(vars).
- comp sum= t(clu)*vars.
- comp w= sscp(vars)-sscp(clu*(sum/(t(n)*make(1,ncol(vars),1)))).
- comp ssw= trace(w).
- comp w= det(w).
  !if (!fst !and !clu<>!head(!clusol)) !then
-  get s /file= 'temp_KO_cccritv.sav'.
-  comp sst= s(1).
-  comp scatt= s(2).
-  comp s= s(3:nrow(s)).
  !else
-  comp sum= csum(sum).
-  comp scatt= sscp(vars)-t(sum)*sum/nt.
-  comp sst= trace(scatt).
-  comp s= eval(scatt/(nt-1)).
-  comp s= sqrt(s(1:csum(s>0))).
-  comp scatt= det(scatt).
  !ifend
- release vars.
- comp v= s(1:rmin({k-1,nrow(s)})).
- loop i= 2 to nrow(v).
-  comp v(i)= v(i)*v(i-1).
- end loop.
- comp c= (v/k)&**(1/t(1:nrow(v))).
- loop p_= nrow(v) to 1 by -1.
-  comp u= s/c(p_).
- end loop if u(p_)>=1.
- do if p_<nrow(u).
-  comp u_= u((p_+1):nrow(u)).
-  comp u_= csum(u_&*u_/(u_+nt)).
- else.
-  comp u_= 0.
- end if.
- comp ewt= (csum(1/(u(1:p_)+nt))+u_)/cssq(u)*((nt-k)**2/nt)*(1+4/nt).
- comp ccc= ln(ewt/(ssw/sst))*sqrt(nt*p_/2)/(1.001-ewt)&**1.2.
- do if w and scatt.
-  comp ldetrat= ln(scatt/w).
- end if.
-end if.
-save {!quote(!clu),ccc,ldetrat} /out= !quote(!conc('temp_KO_cccritv_',!clu,'.sav')) /vari= clusol ccc ldetrat /strings= clusol.
 !if (!fst !and !clu=!head(!clusol)) !then
- save {sst;scatt;s} /out= 'temp_KO_cccritv.sav'.
 !ifend
-end matrix.
!doend
set results list.
add files /file= !quote(!conc('temp_KO_cccritv_',!head(!clusol),'.sav')). 
!do !clu !in (!tail(!clusol))
-add files /file= * /file= !quote(!conc('temp_KO_cccritv_',!clu,'.sav')).
!doend
recode ccc ldetrat (-999=sysmis).
cache.
exec.
var lab clusol 'Cluster solution' ccc 'Cubic clustering criterion' ldetrat 'Log Det Ratio'.
var lev ccc ldetrat (sca).
format ccc ldetrat (f8.3).
list clusol ccc ldetrat.
!if (!tail(!clusol)<>!null) !then
-GRAPH /LINE(SIMPLE)= VALUE(ccc) BY clusol /TITLE= 'Comparison of cluster solutions (Cubic clustering criterion)' /FOOTNOTE= 'The higher the value the "better" is solution'.
-GRAPH /LINE(SIMPLE)= VALUE(ldetrat) BY clusol /TITLE= 'Comparison of cluster solutions (Log Det Ratio)' /FOOTNOTE= 'See bend down ("top elbow")'.
!ifend
!do !clu !in (!clusol)
-erase file !quote(!conc('temp_KO_cccritv_',!clu,'.sav')).
!doend
!if (!fst) !then
-erase file 'temp_KO_cccritv.sav'.
!ifend
!enddefine.

*/*!KO_CCCRITM*/vers1***********************************************.
define !KO_cccritm(matrix= !charend('/') /square= !charend('/') /clusol= !charend('/') /fast= !charend('/'))
echo ''.
echo '**** CUBIC CLUSTERING CRITERION (INPUT - MATRIX) ****'.
!if (!fast='YES' !and !tail(!clusol)<>!null) !then !let !fst= 1 !else !let !fst= 0 !ifend
!let !matr= !matrix !let !fcrop= 0 !let !lcrop= 0
!if (!substr(!matr,1,1)='?') !then !let !matr= !substr(!matr,2) !let !fcrop= 1 !ifend
!let !lm= !len(!matr)
!if (!substr(!matr,!lm,1)='?') !then !let !matr= !substr(!conc(' ',!matr),1,!lm) !let !lcrop= 1 !ifend
set mxloops 100000.
exec.
comp casenum#= $casenum.
!do !clu !in (!clusol)
 !if (!clu=!head(!tail(!clusol))) !then
- set results none.
 !ifend
-temp.
-select if not miss(!clu) and !clu<>0.
-matrix.
-get d /vari= casenum# !matr.
 !if (!fcrop) !then
- comp d= d(:,{1,3:ncol(d)}).
 !ifend
 !if (!lcrop) !then
- comp d= d(:,1:(ncol(d)-1)).
 !ifend
-comp d= d(:,d(:,1)+1).
 !if (!clu=!head(!clusol)) !then
- do if d(1,1)<>0.
-  print /title '>ERROR. Diagonal elements in matrix are not zero. Demanded is distance matrix.'.
-  print /title '>Possibly, you input similarity matrix.' /space= 0.
-  print /title '>!KO_CCCRITM will continue but the result is expected to be invalid.' /space= 0.
- end if.
 !ifend
 !if (!square='YES') !then
- comp d= d&*d.
 !ifend
-get clu /var= !clu.
-do if cmin(clu)=cmax(clu).
- comp ccc= -999.
-else.
- comp nt= nrow(clu).
- comp clu= design(clu).
- comp k= ncol(clu).
- comp n= csum(clu).
- comp ssw= csum(diag(t(clu)*d*clu)/t(2*n)).
  !if (!fst !and !clu<>!head(!clusol)) !then
-  get s /file= 'temp_KO_cccritm.sav'.
-  comp sst= s(1).
-  comp s= s(2:nrow(s)).
  !else
-  comp rmean= (rsum(d)/nt)*make(1,nt,1).
-  comp d= (rmean+t(rmean)-d-msum(d)/nt**2)/2.
-  comp s= eval(d).
-  comp s= s(1:csum(s>1E-13)).
-  comp sst= csum(s).
-  comp s= sqrt(s/(nt-1)).
-  release d.
  !ifend
- comp v= s(1:rmin({k-1,nrow(s)})).
- loop i= 2 to nrow(v).
-  comp v(i)= v(i)*v(i-1).
- end loop.
- comp c= (v/k)&**(1/t(1:nrow(v))).
- loop p_= nrow(v) to 1 by -1.
-  comp u= s/c(p_).
- end loop if u(p_)>=1.
- do if p_<nrow(u).
-  comp u_= u((p_+1):nrow(u)).
-  comp u_= csum(u_&*u_/(u_+nt)).
- else.
-  comp u_= 0.
- end if.
- comp ewt= (csum(1/(u(1:p_)+nt))+u_)/cssq(u)*((nt-k)**2/nt)*(1+4/nt).
- comp ccc= ln(ewt/(ssw/sst))*sqrt(nt*p_/2)/(1.001-ewt)&**1.2.
-end if.
-save {!quote(!clu),ccc} /out= !quote(!conc('temp_KO_cccritm_',!clu,'.sav')) /vari= clusol ccc /strings= clusol.
 !if (!fst !and !clu=!head(!clusol)) !then
- save {sst;s} /out= 'temp_KO_cccritm.sav'.
 !ifend
-end matrix.
!doend
set results list.
add files /file= !quote(!conc('temp_KO_cccritm_',!head(!clusol),'.sav')). 
!do !clu !in (!tail(!clusol))
-add files /file= * /file= !quote(!conc('temp_KO_cccritm_',!clu,'.sav')).
!doend
recode ccc (-999=sysmis).
cache.
exec.
var lab ccc 'Cubic clustering criterion' clusol 'Cluster solution'.
var lev ccc (sca).
format ccc (f8.3).
list clusol ccc.
!if (!tail(!clusol)<>!null) !then
-GRAPH /LINE(SIMPLE)= VALUE(ccc) BY clusol /TITLE= 'Comparison of cluster solutions (Cubic clustering criterion)' /FOOTNOTE= 'The higher the value the "better" is solution'.
!ifend
!do !clu !in (!clusol)
-erase file !quote(!conc('temp_KO_cccritm_',!clu,'.sav')).
!doend
!if (!fst) !then
-erase file 'temp_KO_cccritm.sav'.
!ifend
!enddefine.

*/*!KO_RATLAN*/vers2***********************************************.
define !KO_ratlan(scavars= !charend('/') /catvars= !charend('/') /clusol= !charend('/') /univar= !charend('/'))
echo ''.
echo '**** RATKOWSKY-LANCE CLUSTERING CRITERION ****'.
!let !vlist= !null
!do !var !in (!conc(!scavars,' ',!catvars))
 !if (!upcase(!var)= 'TO') !then
  !let !vlist= !substr(!conc(' ',!vlist),1,!len(!vlist)) !let !vlist= !conc(!vlist,' to ')
 !else
  !let !vlist= !conc(!vlist,!var,',')
 !ifend
!doend
!let !vlist= !substr(!conc(' ',!vlist),1,!len(!vlist))
set mxloops 100000 results none.
exec.
!do !clu !in (!clusol)
-temp.
-select if not miss(!clu) and !clu<>0.
-select if nmiss(!vlist)=0.
-matrix.
-get clu /var= !clu.
-do if cmin(clu)=cmax(clu).
- comp ratlan= -999.
-else.
- comp clu= design(clu).
- comp n= csum(clu).
- comp rk= sqrt(ncol(clu)).
- comp ratlan= 1.
  !if (!scavars<>!null) !then
-  get vars /vari= !scavars /names= scanam.
-  do if any(cmin(vars)=cmax(vars)).
-   comp ratlan= -999.
-  else.
-   comp sum= t(vars)*clu.
-   comp ss= t(vars&*vars)*clu.
-   comp ssb= rsum(sum&*sum/n(make(1,ncol(vars),1),:))-rsum(sum)&**2/nrow(clu).
-   comp sst= rsum(ss)-rsum(sum)&**2/nrow(clu).
-   comp eta= sqrt(ssb/sst)/rk.
-  end if.
  !ifend
  !if (!catvars<>!null) !then
-  do if ratlan<>-999.
-   get vars /vari= !catvars /names= catnam.
-   do if any(cmin(vars)=cmax(vars)).
-    comp ratlan= -999.
-   else.
-    comp crv= make(ncol(vars),1,0).
-    loop i= 1 to ncol(vars).
-     comp desi= design(vars(:,i)).
-     comp tabi= t(desi)*clu.
-     comp chsq= msum(tabi&*tabi/(rsum(tabi)*csum(tabi)/nrow(clu)))-nrow(clu).
-     comp crv(i)= sqrt(chsq/(nrow(clu)*(rmin({ncol(desi),ncol(clu)})-1))).
-    end loop.
-    comp crv= crv/rk.
-   end if.
-  end if.
  !ifend
- do if ratlan<>-999.
-  !if (!scavars<>!null !and !catvars<>!null) !then !let !uni= ',t(eta),t(crv)' !let !nam= ',scanam,catnam'
-   comp ratlan= (csum(eta)+csum(crv))/(nrow(eta)+nrow(crv)).
-  !else !if (!scavars<>!null) !then !let !uni= ',t(eta)' !let !nam= ',scanam'
-   comp ratlan= csum(eta)/nrow(eta).
-  !else !let !uni= ',t(crv)' !let !nam= ',catnam'
-   comp ratlan= csum(crv)/nrow(crv).
   !ifend !ifend
- end if.
-end if.
 !if (!univar='YES') !then
- do if ratlan<>-999.
-  comp nam= {'clusol','ratlan' !nam}.
-  save {!quote(!clu),ratlan !uni} /out= !quote(!conc('temp_KO_ratlan_',!clu,'.sav')) /names= nam /strings= clusol.
- else.
-  save {!quote(!clu),ratlan} /out= !quote(!conc('temp_KO_ratlan_',!clu,'.sav')) /vari= clusol ratlan /strings= clusol.
- end if.
 !else
- save {!quote(!clu),ratlan} /out= !quote(!conc('temp_KO_ratlan_',!clu,'.sav')) /vari= clusol ratlan /strings= clusol.
 !ifend
-end matrix.
!doend
set results list.
add files /file= !quote(!conc('temp_KO_ratlan_',!head(!clusol),'.sav')). 
!do !clu !in (!tail(!clusol))
-add files /file= * /file= !quote(!conc('temp_KO_ratlan_',!clu,'.sav')).
!doend
recode ratlan (-999=sysmis).
cache.
exec.
var lab ratlan 'Ratkowsky-Lance' clusol 'Cluster solution'.
var lev all (sca) /clusol (nom).
format ratlan (f8.3).
list clusol ratlan.
!if (!tail(!clusol)<>!null) !then
-GRAPH /LINE(SIMPLE)= VALUE(ratlan) BY clusol /TITLE= 'Comparison of cluster solutions (Ratkowsky-Lance)' /FOOTNOTE= 'The higher the value the "better" is solution'.
!ifend
!do !clu !in (!clusol)
-erase file !quote(!conc('temp_KO_ratlan_',!clu,'.sav')).
!doend
!enddefine.

*/*!KO_RPBCLU*/vers2***********************************************.
define !KO_rpbclu(matrix= !charend('/') /clusol= !charend('/'))
echo ''.
echo '**** POINT-BISERIAL CORRELATION, MCCLAIN-RAO CLUSTERING CRITERIA ****'.
!let !matr= !matrix !let !fcrop= 0 !let !lcrop= 0
!if (!substr(!matr,1,1)='?') !then !let !matr= !substr(!matr,2) !let !fcrop= 1 !ifend
!let !lm= !len(!matr)
!if (!substr(!matr,!lm,1)='?') !then !let !matr= !substr(!conc(' ',!matr),1,!lm) !let !lcrop= 1 !ifend
set results none.
exec.
comp casenum#= $casenum.
!do !clu !in (!clusol)
-temp.
-select if not miss(!clu) and !clu<>0.
-matrix.
-get d /vari= casenum# !matr.
 !if (!fcrop) !then
- comp d= d(:,{1,3:ncol(d)}).
 !ifend
 !if (!lcrop) !then
- comp d= d(:,1:(ncol(d)-1)).
 !ifend
-comp d= d(:,d(:,1)+1).
-do if d(1,1).
- call setdiag(d,0).
- comp d= -d.
- comp d= d-mmin(d).
-end if.
-call setdiag(d,0).
-get clu /var= !clu.
-do if cmin(clu)=cmax(clu).
- comp r= -999.
- comp mcr= -999.
-else.
- comp clu= clu*make(1,nrow(clu),1). 
- comp clu= (clu=t(clu)).
- call setdiag(clu,0).
- comp sw= msum(d&*clu)/2.
- comp sb= msum(d)/2-sw.
- comp nw= msum(clu).
- comp nb= nrow(clu)**2-nrow(clu)-nw.
- comp ssdev= sqrt((mssq(d)-msum(d)&**2/(nw+nb))/2).
- comp r= (sb/nb-sw/nw)/ssdev*sqrt(nb*nw*2/(nb+nw)).
- do if sb.
-  comp mcr= (sw/sb)*(nb/nw).
- else.
-  comp mcr= -999.
- end if.
-end if.
-save {!quote(!clu),r,mcr} /out= !quote(!conc('temp_KO_rpbclu_',!clu,'.sav')) /vari= clusol r_pb mcr /strings= clusol.
-end matrix.
!doend
del var casenum#.
set results list.
add files /file= !quote(!conc('temp_KO_rpbclu_',!head(!clusol),'.sav')). 
!do !clu !in (!tail(!clusol))
-add files /file= * /file= !quote(!conc('temp_KO_rpbclu_',!clu,'.sav')).
!doend
recode r_pb mcr (-999=sysmis).
cache.
exec.
var lab r_pb 'Point-biserial r' mcr 'McClain-Rao' clusol 'Cluster solution'.
var lev r_pb mcr (sca).
format r_pb mcr (f8.3).
list clusol r_pb mcr.
!if (!tail(!clusol)<>!null) !then
-GRAPH /LINE(SIMPLE)= VALUE(r_pb) BY clusol /TITLE= 'Comparison of cluster solutions (r)' /FOOTNOTE= 'The higher the value the "better" is solution'.
-GRAPH /LINE(SIMPLE)= VALUE(mcr) BY clusol /TITLE= 'Comparison of cluster solutions (McClain-Rao)' /FOOTNOTE= 'See bend up ("bottom elbow")'.
!ifend
!do !clu !in (!clusol)
-erase file= !quote(!conc('temp_KO_rpbclu_',!clu,'.sav')).
!doend
!enddefine.

*/*!KO_GAMMACLU*/vers2***********************************************.
define !KO_gammaclu(matrix= !charend('/') /rescrnd= !charend('/') /clusol= !charend('/'))
echo ''.
echo '**** GAMMA STATISTIC CLUSTERING CRITERION ****'.
!if (!rescrnd<>!null !and !rescrnd<>1 !and !rescrnd<>2 !and !rescrnd<>3 !and !rescrnd<>4 !and !rescrnd<>5) !then
-echo ''.
-echo '>ERROR. RESCRND, if specified, can only be digit 1, 2, 3, 4, or 5.'.
-echo '>!KO_GAMMACLU is not executed.'.
!else
 !let !matr= !matrix !let !fcrop= 0 !let !lcrop= 0
 !if (!substr(!matr,1,1)='?') !then !let !matr= !substr(!matr,2) !let !fcrop= 1 !ifend
 !let !lm= !len(!matr)
 !if (!substr(!matr,!lm,1)='?') !then !let !matr= !substr(!conc(' ',!matr),1,!lm) !let !lcrop= 1 !ifend
-set mxloops 100000 results none.
-exec.
-comp casenum#= $casenum.
 !do !clu !in (!clusol)
- temp.
- select if not miss(!clu) and !clu<>0.
- matrix.
- get d /vari= casenum# !matr.
  !if (!fcrop) !then
-  comp d= d(:,{1,3:ncol(d)}).
  !ifend
  !if (!lcrop) !then
-  comp d= d(:,1:(ncol(d)-1)).
  !ifend
- comp d= d(:,d(:,1)+1).
- do if not d(1,1).
-  comp d= -d.
- end if.
- get clu /var= !clu.
- do if cmin(clu)=cmax(clu).
-  comp gamma= -999.
-  comp n#$= -999.
- else.
-  comp clu= clu*make(1,nrow(clu),1).
-  comp clu= clu=t(clu).
-  comp n= ncol(d).
-  comp d= reshape(d,n**2,1).
-  comp clu= reshape(clu,n**2,1).
-  comp uind= make(1,n*(n-1)/2,0).
-  loop i= 1 to n-1.
-   comp st= -(n-1)+(n+.5)*i-.5*i**2.
-   comp ind= i*n+(1-n+i):0.
-   comp uind(st:(st+ncol(ind)-1))= ind.
-  end loop.
-  comp d= d(uind).
-  comp clu= clu(uind).
-  release uind,ind.
   !if (!rescrnd<>!null) !then
-   comp min= mmin(d).
-   comp d= rnd(!conc('1E',!rescrnd)/(mmax(d)-min)&*(d-min)).
   !ifend
-  comp rdes= design(d).
-  comp n#$= ncol(rdes).
-  comp cdes= design(clu).
-  comp tab= t(rdes)*cdes.
-  comp rlist= d*make(1,ncol(rdes),1)&*rdes.
-  comp rlist= cmax(rlist)+cmin(rlist).
-  comp clist= clu*make(1,ncol(cdes),1)&*cdes.
-  comp clist= cmax(clist)+cmin(clist).
-  comp rgrd= grade(rlist).
-  comp cgrd= grade(clist).
-  comp tab(rgrd,cgrd)= tab.
-  release d,clu,rdes,cdes,rlist,clist,rgrd,cgrd.
-  comp c= make(nrow(tab),ncol(tab),0).
-  comp c(1:(nrow(tab)-1),1:(ncol(tab)-1))= tab(2:nrow(tab),2:ncol(tab)).
-  loop i= nrow(c)-1 to 1 by -1.
-   comp c(i,:)= c(i,:)+c((i+1),:).
-  end loop.
-  comp c= t(c).
-  loop i= nrow(c)-1 to 1 by -1.
-   comp c(i,:)= c(i,:)+c((i+1),:).
-  end loop.
-  comp p= msum(tab&*t(c)).
-  comp c= make(nrow(tab),ncol(tab),0).
-  comp c(1:(nrow(tab)-1),2:ncol(tab))= tab(2:nrow(tab),1:(ncol(tab)-1)).
-  loop i= nrow(c)-1 to 1 by -1.
-   comp c(i,:)= c(i,:)+c((i+1),:).
-  end loop.
-  comp c= t(c).
-  loop i= 2 to nrow(c).
-   comp c(i,:)= c(i,:)+c((i-1),:).
-  end loop.
-  comp q= msum(tab&*t(c)).
-  comp gamma= (p-q)/(p+q).
- end if.
- save {!quote(!clu),gamma,n#$} /out= !quote(!conc('temp_KO_gammaclu_',!clu,'.sav')) /vari= clusol gamma n#$ /strings= clusol.
- end matrix.
 !doend
-del var casenum#.
-set results list.
-add files /file= !quote(!conc('temp_KO_gammaclu_',!head(!clusol),'.sav')). 
 !do !clu !in (!tail(!clusol))
- add files /file= * /file= !quote(!conc('temp_KO_gammaclu_',!clu,'.sav')).
 !doend
-recode gamma n#$ (-999=sysmis).
-cache.
-exec.
-var lab gamma 'Gamma' clusol 'Cluster solution' n#$ 'Number of unique values of proximities'.
-var lev gamma (sca).
-format gamma (f8.3) n#$ (f8).
-list clusol gamma n#$.
 !if (!tail(!clusol)<>!null) !then
- GRAPH /LINE(SIMPLE)= VALUE(gamma) BY clusol /TITLE= 'Comparison of cluster solutions (Gamma)' /FOOTNOTE= 'The higher the value the "better" is solution'.
 !ifend
 !do !clu !in (!clusol)
- erase file= !quote(!conc('temp_KO_gammaclu_',!clu,'.sav')).
 !doend
!ifend
!enddefine.

*/*!KO_CINDEX*/vers1***********************************************.
define !KO_cindex(matrix= !charend('/') /clusol= !charend('/') /fast= !charend('/'))
echo ''.
echo '**** C-INDEX CLUSTERING CRITERION ****'.
!if (!fast='YES' !and !tail(!clusol)<>!null) !then !let !fst= 1 !else !let !fst= 0 !ifend
!let !matr= !matrix !let !fcrop= 0 !let !lcrop= 0
!if (!substr(!matr,1,1)='?') !then !let !matr= !substr(!matr,2) !let !fcrop= 1 !ifend
!let !lm= !len(!matr)
!if (!substr(!matr,!lm,1)='?') !then !let !matr= !substr(!conc(' ',!matr),1,!lm) !let !lcrop= 1 !ifend
set mxloops 100000 results none.
exec.
comp casenum#= $casenum.
!do !clu !in (!clusol)
-temp.
-select if not miss(!clu) and !clu<>0.
-matrix.
-get d /vari= casenum# !matr.
 !if (!fcrop) !then
- comp d= d(:,{1,3:ncol(d)}).
 !ifend
 !if (!lcrop) !then
- comp d= d(:,1:(ncol(d)-1)).
 !ifend
-comp d= d(:,d(:,1)+1).
-do if d(1,1).
- call setdiag(d,0).
- comp d= -d.
- comp d= d-mmin(d).
-end if.
-call setdiag(d,0).
-get clu /var= !clu.
-do if cmin(clu)=cmax(clu).
- comp cindex= -999.
-else.
- comp clu= clu*make(1,nrow(clu),1). 
- comp clu= (clu=t(clu)).
- call setdiag(clu,0).
- comp sw= msum(d&*clu)/2.
- comp nw= msum(clu).
- comp nb= nrow(clu)**2-nrow(clu)-nw.
  !if (!fst !and !clu<>!head(!clusol)) !then
-  get dvec /file= 'temp_KO_cindex.sav'.
-  comp rdvec= dvec(:,2).
-  comp dvec= dvec(:,1).
  !else
-  comp n= ncol(d).
-  comp dvec= reshape(d,n**2,1).
-  comp uind= make(1,n*(n-1)/2,0).
-  loop i= 1 to n-1.
-   comp st= -(n-1)+(n+.5)*i-.5*i**2.
-   comp ind= i*n+(1-n+i):0.
-   comp uind(st:(st+ncol(ind)-1))= ind.
-  end loop.
-  comp dvec= dvec(uind). /*Развернули, dvec это столбец, верх треуг матрицы d
-  release uind,ind,d.
-  comp rdvec= grade(dvec).
  !ifend
- comp minsw= csum(dvec&*(rdvec<=nw/2)).
- comp maxsw= csum(dvec&*(rdvec>nb/2)).
- comp cindex= (sw-minsw)/(maxsw-minsw).
-end if.
-save {!quote(!clu),cindex} /out= !quote(!conc('temp_KO_cindex_',!clu,'.sav')) /vari= clusol c_index /strings= clusol.
 !if (!fst !and !clu=!head(!clusol)) !then
- save {dvec,rdvec} /out= 'temp_KO_cindex.sav'.
 !ifend
-end matrix.
!doend
del var casenum#.
set results list.
add files /file= !quote(!conc('temp_KO_cindex_',!head(!clusol),'.sav')). 
!do !clu !in (!tail(!clusol))
-add files /file= * /file= !quote(!conc('temp_KO_cindex_',!clu,'.sav')).
!doend
recode c_index (-999=sysmis).
cache.
exec.
var lab c_index 'C-Index' clusol 'Cluster solution'.
var lev c_index (sca).
format c_index (f8.3).
list clusol c_index.
!if (!tail(!clusol)<>!null) !then
-GRAPH /LINE(SIMPLE)= VALUE(c_index) BY clusol /TITLE= 'Comparison of cluster solutions (C-Index)' /FOOTNOTE= 'The lower the value the "better" is solution'.
!ifend
!do !clu !in (!clusol)
-erase file= !quote(!conc('temp_KO_cindex_',!clu,'.sav')).
!doend
!if (!fst) !then
-erase file 'temp_KO_cindex.sav'.
!ifend
!enddefine.

*/*!KO_DUNN*/vers2***********************************************.
define !KO_dunn(matrix= !charend('/') /clusol= !charend('/') /distb= !charend('/') /diamw= !charend('/'))
echo ''.
echo '**** DUNN CLUSTERING CRITERION ****'.
!let !matr= !matrix !let !fcrop= 0 !let !lcrop= 0
!if (!substr(!matr,1,1)='?') !then !let !matr= !substr(!matr,2) !let !fcrop= 1 !ifend
!let !lm= !len(!matr)
!if (!substr(!matr,!lm,1)='?') !then !let !matr= !substr(!conc(' ',!matr),1,!lm) !let !lcrop= 1 !ifend
set mxloops 100000 results none.
exec.
comp casenum#= $casenum.
!do !clu !in (!clusol)
-temp.
-select if not miss(!clu) and !clu<>0.
-matrix.
-get d /vari= casenum# !matr.
 !if (!fcrop) !then
- comp d= d(:,{1,3:ncol(d)}).
 !ifend
 !if (!lcrop) !then
- comp d= d(:,1:(ncol(d)-1)).
 !ifend
-comp d= d(:,d(:,1)+1).
-do if d(1,1).
- call setdiag(d,0).
- comp d= -d.
- comp d= d-mmin(d).
-end if.
-call setdiag(d,0).
-comp dunn= -999.
-get clu /var= !clu.
-do if cmin(clu)<cmax(clu).
- comp des= design(clu).
- comp n= csum(des).
  !if (!distb='SINGLE' !or !distb=!null) !then !let !tp1= 'SINGLE'
-  comp clu= clu*make(1,nrow(clu),1). 
-  comp clu= (clu=t(clu)).
-  comp mindb= mmin(d&*(not clu)+clu*1E300).
  !else !let !tp1= !distb
   !if (!distb='BAVERAGE' !or !distb='CENTROID' !or !distb='CROSSPC') !then
-   comp nn= sscp(n).
    !if (!distb='BAVERAGE') !then
-    comp m= t(des)*d*des/nn.
    !else !if (!distb='CENTROID') !then !let !sqrt= 'sqrt'
-    comp m= t(des)*(d&*d)*des.
-    comp x= diag(m)/2*n&**2.
-    comp m= (m-(x+t(x))/nn)/nn.
    !else
-    comp m2= d&*d*des.
-    comp ones= make(nrow(des),1,1).
-    comp x= csum(m2&*des)/2.
-    comp m2= (m2-(x/n)(ones,:))/n(ones,:).
-    comp m= t(des)*sqrt(m2).
-    release m2.
-    comp nn= n(make(1,ncol(des),1),:).
-    comp m= (m+t(m))/(nn+t(nn)).
   !ifend !ifend
-   call setdiag(m,1E300).
-   comp mindb= !sqrt(mmin(m)).
  !else
-   comp @m= des.
-   loop @i= 2 to nrow(@m).
-    comp @m(@i,:)= @m(@i-1,:)+@m(@i,:).
-   end loop.
-   comp @m= @m&*des.
-   comp inds= make(nrow(@m)+1,ncol(@m),0).
-   comp @nums= t(1:nrow(@m)).
-   comp @m= @m+(not @m)*nrow(inds).
-   loop @i= 1 to ncol(@m).
-    comp inds(@m(:,@i),@i)= @nums.
-   end loop.
-   comp inds= inds(1:(nrow(inds)-1),:).
-   release @m,@nums.
-   comp mindb= mmax(d).
-   loop i= 1 to ncol(des)-1.
-    loop j= i+1 to ncol(des).
      !if (!distb='COMPLETE') !then
-      comp far= mmax(d(inds(1:n(i),i),inds(1:n(j),j))).
-      comp mindb= rmin({mindb,far}).
      !else
-      comp m= d(inds(1:n(i),i),inds(1:n(j),j)).
-      comp haus= rmax({cmax(rmin(m)),rmax(cmin(m))}).
-      comp mindb= rmin({mindb,haus}).
      !ifend
-    end loop.
-   end loop.
   !ifend
  !ifend
  !if (!diamw='MXDIST' !or !diamw=!null) !then !let !tp2= 'MXDIST'
   !if (!distb<>'SINGLE' !and !distb<>!null) !then
-   comp clu= clu*make(1,nrow(clu),1).
-   comp clu= (clu=t(clu)).
   !ifend
-  comp mxdiam= mmax(d&*clu).
  !else !let !tp2= !diamw
   !if (!diamw='AVDIST') !then
-   comp mxdiam= cmax(diag(t(des)*d*des)/t(n&*(n-(n>1)))).
   !else
-   comp m= d&*d*des.
-   comp ones= make(nrow(des),1,1).
-   comp x= csum(m&*des)/2.
-   comp m= (m-(x/n)(ones,:))/n(ones,:).
-   comp mxdiam= t(des)*sqrt(m).
-   comp mxdiam= cmax(2*diag(mxdiam)/t(n)).
   !ifend
  !ifend
- do if mxdiam>0.
-  comp dunn= mindb/mxdiam.
- end if.
-end if.
-save {!quote(!clu),dunn} /out= !quote(!conc('temp_KO_dunn_',!clu,'.sav')) /vari= clusol dunn /strings= clusol.
-end matrix.
!doend
del var casenum#.
set results list.
add files /file= !quote(!conc('temp_KO_dunn_',!head(!clusol),'.sav')). 
!do !clu !in (!tail(!clusol))
-add files /file= * /file= !quote(!conc('temp_KO_dunn_',!clu,'.sav')).
!doend
recode dunn (-999=sysmis).
cache.
exec.
var lab dunn 'Dunn' clusol 'Cluster solution'.
var lev dunn (sca).
format dunn (f8.3).
list clusol dunn. 
!if (!tail(!clusol)<>!null) !then
-GRAPH /LINE(SIMPLE)= VALUE(dunn) BY clusol /TITLE= !quote(!conc('Comparison of cluster solutions (Dunn, type ',!tp1,'+',!tp2,')'))
       /FOOTNOTE= 'The higher the value the "better" is solution'.
!ifend
!if (!distb='CENTROID' !or !distb='CROSSPC' !or !diamw='AVDEV') !then
-echo '>REMINDER. With DISTB=CENTROID/CROSSPC or DIAMW=AVDEV the input matrix must'.
-echo ' be of non-squared euclidean distances or distances that approach'.
-echo " geometrically to being such. Only in this case the macro's results make sense,".
-echo ' for then one could speak of cluster centroids.'.
!ifend
!do !clu !in (!clusol)
-erase file= !quote(!conc('temp_KO_dunn_',!clu,'.sav')).
!doend
!enddefine.

*/*!KO_SILHOU*/vers3***********************************************.
define !KO_silhou(matrix= !charend('/') /clusol= !charend('/') /type= !charend('/') /caps= !charend('/') /single= !charend('/')
               /widorig= !charend('/') /widcoll= !charend('/') /coef= !charend('/') !default(1.41421356))
echo ''.
echo '**** SILHOUETTE STATISTIC CLUSTERING CRITERION ****'.
exec.
!if (!single=!null !or !caps=!null) !then
-echo ''.
-echo '>ERROR. Not specified CAPS or SINGLE.'.
!else !if (!single='ASSEEDS' !and (!type='NEAR' !or !type='FARTH' !or !type='FARNEAR')) !then
-echo ''.
-echo '>ERROR. SINGLE=ASSEEDS is incompatible with TYPE=NEAR, FARTH, or FARNEAR.'.
!else
 !let !bclu= !unquote(!head(!caps)) !let !sil= !unquote(!head(!tail(!caps)))
 !let !matr= !matrix !let !fcrop= 0 !let !lcrop= 0
 !if (!substr(!matr,1,1)='?') !then !let !matr= !substr(!matr,2) !let !fcrop= 1 !ifend
 !let !lm= !len(!matr)
 !if (!substr(!matr,!lm,1)='?') !then !let !matr= !substr(!conc(' ',!matr),1,!lm) !let !lcrop= 1 !ifend
-set mxloops 100000 results none.
-comp casenum#= $casenum.
 !do !clu !in (!clusol)
- temp.
- select if not miss(!clu) and !clu<>0.
- matrix.
- get d /vari= casenum# !matr.
  !if (!fcrop) !then
-  comp d= d(:,{1,3:ncol(d)}).
  !ifend
  !if (!lcrop) !then
-  comp d= d(:,1:(ncol(d)-1)).
  !ifend
- comp case= d(:,1).
- comp d= d(:,d(:,1)+1).
- do if d(1,1).
-  call setdiag(d,0).
-  comp d= -d.
-  comp d= d-mmin(d).
- end if.
- call setdiag(d,0).
- get clu /var= !clu.
- do if cmin(clu)=cmax(clu).
-  comp bclu= make(nrow(clu),1,-999).
-  comp sil= make(nrow(clu),1,-999).
- else.
-  comp des= design(clu).
-  comp n= csum(des).
-  comp ones= make(nrow(des),1,1).
   !if (!type='AVER' !or !type=!null) !then
-   comp n_= n(ones,:)-des.
-   comp abmx= d*des/(n_+(n_=0)).
   !else !if (!type='DEVIAT') !then !let !div2= '/2'
-   comp abmx2= d&*d*des.
-   comp x= csum(abmx2&*des)/2.
-   comp abmx= sqrt((abmx2-(x/n)(ones,:))/n(ones,:)).
-   release abmx2.
   !else !if (!type='FARTH') !then
-   comp abmx= make(nrow(des),ncol(des),0).
-   loop i= 1 to ncol(des).
-    comp abmx(:,i)= rmax(d&*t(des(:,i))(ones,:)).
-   end loop.
   !else !if (!type='NEAR') !then
-   comp abmx= make(nrow(des),ncol(des),0).
-   comp bigval= 1E300.
-   call setdiag(d,bigval).
-   loop i= 1 to ncol(des).
-    comp abmx(:,i)= rmin(d+bigval*t(not des(:,i))(ones,:)).
-   end loop.
-   comp abmx= abmx&*(abmx<>bigval).
-   call setdiag(d,0).
   !else
-   comp abmx= make(nrow(des),ncol(des),0).
-   comp abmx2= make(nrow(des),ncol(des),0).
-   comp bigval= mmax(d)+1.
-   loop i= 1 to ncol(des).
-    comp tdesi= t(des(:,i)).
-    comp abmx(:,i)= rmax(d&*tdesi(ones,:)).
-    comp abmx2(:,i)= rmin(d+bigval*(not tdesi)(ones,:)).
-   end loop.
-   comp abmx= abmx&*des+abmx2&*(not des).
-   release abmx2.
   !ifend !ifend !ifend !ifend
-  comp a= rmax(abmx&*des).
-  comp abmx= abmx+des&*1E300.
-  comp b= rmin(abmx).
-  comp codes= t(clu)*des/n.
-  comp mincode= rmin(codes).
-  comp codes= codes-mincode+1.
-  comp bclu= rmax((abmx=b*make(1,ncol(abmx),1))&*codes(ones,:)).
-  comp bclu= bclu+mincode-1.
-  comp n_1= n=1.
-  comp nsing= rsum(n_1).
-  do if nsing.
-   comp sing= des*t(n_1).
    !if (!single='ASSEEDS') !then
-    comp n#= t(n+n_1).
-    comp wamean= diag(t(des)*d*des)/(n#&*(n#-1)).
-    comp n#= 1/n.
-    comp wamean= rsum(t(wamean)&*n#)/rsum(n#-n_1) !div2.
-    comp a= a+wamean&*sing.
    !ifend
-   comp sil= (b-a)/(rmax({a,b})+(rmax({a,b})=0)).
    !if (!single='IFMERGE') !then
-    comp nnsing= nrow(sing)-nsing.
-    comp asum= csum(a).
-    comp amean= asum/nnsing.
-    comp asd= sqrt((cssq(a)-asum&*amean)/nnsing).
-    comp silmean= csum(sil&*(not sing))/nnsing.
-    comp sil= ((silmean+1)/(1+exp(-(b-amean)/(!coef*asd)))-1)&*sing+sil&*(not sing).
    !ifend
    !if (!single<>'ASSEEDS' !and !single<>'IFMERGE') !then !let !recsil= !conc('(2=',!single,')')
-    comp sil= 2*sing+sil&*(not sing).
    !ifend
-  else.
-   comp sil= (b-a)/(rmax({a,b})+(rmax({a,b})=0)).
-  end if.
- end if.
- save {case,bclu,sil} /out= !quote(!conc('temp_KO_silhou_',!clu,'.sav')) /vari= casenum# bclu sil.
- end matrix.
- set errors none.
- del vari !conc(!bclu,!clu) !conc(!sil,!clu).
- set errors list.
 !doend
-set results list.
 !if (!type=!null) !then !let !tp= 'type=AVER' !else !let !tp= !conc('type=',!type) !ifend
-dataset copy temp#$$$$$._ window= hidden.
 !do !clu !in (!clusol) !let !silclu= !conc(!sil,!clu) !let !bcluclu= !conc(!bclu,!clu)
- match files file= * /file= !quote(!conc('temp_KO_silhou_',!clu,'.sav')) /rename= (bclu sil = !bcluclu !silclu) /by casenum#.
- recode !silclu !recsil (-999=sysmis) /!bcluclu (-999=sysmis).
- var lab !bcluclu !quote(!conc('Closest cluster, ',!tp)).
- var lev !bcluclu (nom) /!silclu (sca).
 !doend
-cache.
-descr vari= !do !clu !in (!clusol) !conc(!sil,!clu) !doend /stat= mean stddev.
-apply dict from temp#$$$$$._ /fileinfo weight.
-dataset close temp#$$$$$._.
-del var casenum#.
 !if (!tail(!clusol)<>!null) !then
- GRAPH /LINE(SIMPLE)= MEAN(!do !clu !in (!clusol) !conc(!sil,!clu) !doend) /INTERVAL SD(1) /MISSING= VARIABLE
       /TITLE= !quote(!conc('Comparison of cluster solutions by Silhouette Statistic ',!tp)) /FOOTNOTE= 'The higher the mean the "better" is solution'.
 !else
- rank vari= !conc(!sil,!clusol) (d) by !clusol /rank into rsil$_.# /ties= condense /print= no.
- do if $casenum=1.
  !if (!widorig=!null) !then !let !wdrg= 0 !else !let !wdrg= !widorig !ifend
  !if (!widcoll=!null) !then !let !wdcl= 5 !else !let !wdcl= !widcoll !ifend
-  write outfile= 'temp_KO_silhou_0.sps'
    /'SOURCE: s= userSource(id("graphdataset"))'
    /'DATA: rsil= col(source(s),name("rsil$_.#"))'
    /!quote(!conc('DATA: sil= col(source(s),name("',!sil,!clusol,'"))'))
    /!quote(!conc('DATA: clu= col(source(s),name("',!clusol,'"),unit.category())'))
    /'DATA: COUNT= col(source(s),name("COUNT"))'
    /'COORD: rect(dim(1,2),wrap())'
    /'GUIDE: axis(dim(1),ticks(null()))'
    /'GUIDE: axis(dim(2),delta(0.2),label("value Silhouette"))'
    /'GUIDE: axis(dim(3),opposite())'
    /!quote(!conc('GUIDE: text.title(label("','Silhouette width: ',!clusol,' (',!tp,')','"))'))
    /'GUIDE: text.footnote(label("Objects (axis X) are sorted in clusters descendingly of value Silhouette"))'
    /!quote(!conc('SCALE: linear(dim(2),min(-1),max(1),origin(',!wdrg,'))'))
    /!quote(!conc('TRANS: cluTRANS= collapse(category(clu),minimumPercent(',!wdcl,'),sumVariable(COUNT),otherValue("Other (Silhouette mix)"))'))
    /'ELEMENT: area(position(smooth.step.left(rsil*sil*cluTRANS)),missing.wings())'
    /'ELEMENT: point(position(rsil*sil*cluTRANS),shape.interior(shape.circle),size(size.tiny))'.
- end if.
- frequ !clusol.
- GGRAPH
   /GRAPHDATASET NAME= "graphdataset" VARIABLES= rsil$_.# !conc(!sil,!clusol) !clusol
    COUNT() MISSING= LISTWISE REPORTMISSING= NO
   /GRAPHSPEC SOURCE= GPLFILE('temp_KO_silhou_0.sps').
- del var rsil$_.#.
- erase file 'temp_KO_silhou_0.sps'.
 !ifend
 !if (!type='DEVIAT') !then
- echo '>REMINDER. With TYPE=DEVIAT input matrix must be of non-squared euclidean'.
- echo ' distances or of distances that approach geometrically to being such.'.
- echo " Only in this case the macro's results make sense, for then one could speak".
- echo ' of cluster centroids.'.
 !ifend
-dataset decl m$._sil# window= hidden.
-aggr /out= m$._sil#
      /!do !clu !in (!clusol) !clu !doend = mean(!do !clu !in (!clusol) !conc(!sil,!clu) !doend).
-dataset activ m$._sil#.
-set results none.
-flip all.
-set results list.
-dataset close m$._sil#.
-rename vari (all = clusol silhou).
-var lab silhou 'mean Silhouette' clusol 'Cluster solution'.
-var lev silhou (sca).
-format silhou (f8.3).
 !do !clu !in (!clusol)
- erase file= !quote(!conc('temp_KO_silhou_',!clu,'.sav')).
 !doend
!ifend !ifend
!enddefine.

*/*!KO_SILDEV*/vers1***********************************************.
define !KO_sildev(vars= !charend('/') /clusol= !charend('/') /caps= !charend('/') /single= !charend('/')
               /widorig= !charend('/') /widcoll= !charend('/') /coef= !charend('/') !default(1.41421356))
echo ''.
echo '**** SILHOUETTE STATISTIC CLUSTERING CRITERION (DEVIATION TYPE) ****'.
exec.
!if (!single=!null !or !caps=!null) !then
-echo ''.
-echo '>ERROR. Not specified CAPS or SINGLE.'.
!else
 !let !bclu= !unquote(!head(!caps)) !let !sil= !unquote(!head(!tail(!caps)))
-set mxloops 100000 results none.
-comp casenum#= $casenum.
 !do !clu !in (!clusol)
- temp.
- select if not miss(!clu) and !clu<>0.
- matrix.
- get vars /vari= casenum# !clu !vars /miss= omit.
- comp case= vars(:,1).
- comp clu= vars(:,2).
- comp vars= vars(:,3:ncol(vars)).
- do if cmin(clu)=cmax(clu).
-  comp bclu= make(nrow(clu),1,-999).
-  comp sil= make(nrow(clu),1,-999).
- else.
-  comp des= design(clu).
-  comp n= csum(des).
-  comp ones= make(nrow(des),1,1).
-  comp abmx= make(nrow(des),ncol(des),0).
-  comp mean= t(des)*vars/(t(n)*make(1,ncol(vars),1)).
-  loop i= 1 to ncol(des).
-   comp abmx(:,i)= rssq(vars-ones*mean(i,:)).
-  end loop.
-  comp abmx= sqrt(abmx).
-  release vars.
-  comp a= rmax(abmx&*des).
-  comp abmx= abmx+des&*1E300.
-  comp b= rmin(abmx).
-  comp codes= t(clu)*des/n.
-  comp mincode= rmin(codes).
-  comp codes= codes-mincode+1.
-  comp bclu= rmax((abmx=b*make(1,ncol(abmx),1))&*codes(ones,:)).
-  comp bclu= bclu+mincode-1.
-  comp sil= (b-a)/(rmax({a,b})+(rmax({a,b})=0)).
-  comp n_1= n=1.
-  comp nsing= rsum(n_1).
-  do if nsing.
-   comp sing= des*t(n_1).
    !if (!single='IFMERGE') !then
-    comp nnsing= nrow(sing)-nsing.
-    comp asum= csum(a).
-    comp amean= asum/nnsing.
-    comp asd= sqrt((cssq(a)-asum&*amean)/nnsing).
-    comp silmean= csum(sil&*(not sing))/nnsing.
-    comp sil= ((silmean+1)/(1+exp(-(b-amean)/(!coef*asd)))-1)&*sing+sil&*(not sing).
    !else !let !recsil= !conc('(2=',!single,')')
-    comp sil= 2*sing+sil&*(not sing).
    !ifend
-  end if.
- end if.
- save {case,bclu,sil} /out= !quote(!conc('temp_KO_sildev_',!clu,'.sav')) /vari= casenum# bclu sil.
- end matrix.
- set errors none.
- del vari !conc(!bclu,!clu) !conc(!sil,!clu).
- set errors list.
 !doend
-set results list.
 !let !tp= 'type=DEVIAT'
-dataset copy temp#$$$$$._ window= hidden.
 !do !clu !in (!clusol) !let !silclu= !conc(!sil,!clu) !let !bcluclu= !conc(!bclu,!clu)
- match files file= * /file= !quote(!conc('temp_KO_sildev_',!clu,'.sav')) /rename= (bclu sil = !bcluclu !silclu) /by casenum#.
- recode !silclu !recsil (-999=sysmis) /!bcluclu (-999=sysmis).
- var lab !bcluclu !quote(!conc('Closest cluster, ',!tp)).
- var lev !bcluclu (nom) /!silclu (sca).
 !doend
-cache.
-descr vari= !do !clu !in (!clusol) !conc(!sil,!clu) !doend /stat= mean stddev.
-apply dict from temp#$$$$$._ /fileinfo weight.
-dataset close temp#$$$$$._.
-del var casenum#.
 !if (!tail(!clusol)<>!null) !then
- GRAPH /LINE(SIMPLE)= MEAN(!do !clu !in (!clusol) !conc(!sil,!clu) !doend) /INTERVAL SD(1) /MISSING= VARIABLE
       /TITLE= !quote(!conc('Comparison of cluster solutions by Silhouette Statistic ',!tp)) /FOOTNOTE= 'The higher the mean the "better" is solution'.
 !else
- rank vari= !conc(!sil,!clusol) (d) by !clusol /rank into rsil$_.# /ties= condense /print= no.
- do if $casenum=1.
  !if (!widorig=!null) !then !let !wdrg= 0 !else !let !wdrg= !widorig !ifend
  !if (!widcoll=!null) !then !let !wdcl= 5 !else !let !wdcl= !widcoll !ifend
-  write outfile= 'temp_KO_sildev_0.sps'
    /'SOURCE: s= userSource(id("graphdataset"))'
    /'DATA: rsil= col(source(s),name("rsil$_.#"))'
    /!quote(!conc('DATA: sil= col(source(s),name("',!sil,!clusol,'"))'))
    /!quote(!conc('DATA: clu= col(source(s),name("',!clusol,'"),unit.category())'))
    /'DATA: COUNT= col(source(s),name("COUNT"))'
    /'COORD: rect(dim(1,2),wrap())'
    /'GUIDE: axis(dim(1),ticks(null()))'
    /'GUIDE: axis(dim(2),delta(0.2),label("value Silhouette"))'
    /'GUIDE: axis(dim(3),opposite())'
    /!quote(!conc('GUIDE: text.title(label("','Silhouette width: ',!clusol,' (',!tp,')','"))'))
    /'GUIDE: text.footnote(label("Objects (axis X) are sorted in clusters descendingly of value Silhouette"))'
    /!quote(!conc('SCALE: linear(dim(2),min(-1),max(1),origin(',!wdrg,'))'))
    /!quote(!conc('TRANS: cluTRANS= collapse(category(clu),minimumPercent(',!wdcl,'),sumVariable(COUNT),otherValue("Other (Silhouette mix)"))'))
    /'ELEMENT: area(position(smooth.step.left(rsil*sil*cluTRANS)),missing.wings())'
    /'ELEMENT: point(position(rsil*sil*cluTRANS),shape.interior(shape.circle),size(size.tiny))'.
- end if.
- frequ !clusol.
- GGRAPH
   /GRAPHDATASET NAME= "graphdataset" VARIABLES= rsil$_.# !conc(!sil,!clusol) !clusol
    COUNT() MISSING= LISTWISE REPORTMISSING= NO
   /GRAPHSPEC SOURCE= GPLFILE('temp_KO_sildev_0.sps').
- del var rsil$_.#.
- erase file 'temp_KO_sildev_0.sps'.
 !ifend
-dataset decl m$._sil# window= hidden.
-aggr /out= m$._sil#
      /!do !clu !in (!clusol) !clu !doend = mean(!do !clu !in (!clusol) !conc(!sil,!clu) !doend).
-dataset activ m$._sil#.
-set results none.
-flip all.
-set results list.
-dataset close m$._sil#.
-rename vari (all = clusol silhou).
-var lab silhou 'mean Silhouette' clusol 'Cluster solution'.
-var lev silhou (sca).
-format silhou (f8.3).
 !do !clu !in (!clusol)
- erase file= !quote(!conc('temp_KO_sildev_',!clu,'.sav')).
 !doend
!ifend
!enddefine.

*/*!KO_AICBIC*/vers1***********************************************.
define !KO_aicbic(scavars= !charend('/') /catvars= !charend('/') /clusol= !charend('/'))
echo ''.
echo '**** AIC & BIC CLUSTERING CRITERIA ****'.
!let !vlist= !null
!do !var !in (!conc(!scavars,' ',!catvars))
 !if (!upcase(!var)= 'TO') !then
  !let !vlist= !substr(!conc(' ',!vlist),1,!len(!vlist)) !let !vlist= !conc(!vlist,' to ')
 !else
  !let !vlist= !conc(!vlist,!var,',')
 !ifend
!doend
!let !vlist= !substr(!conc(' ',!vlist),1,!len(!vlist))
set mxloops 100000 results none.
exec.
!do !clu !in (!clusol)
-temp.
-select if not miss(!clu) and !clu<>0.
-select if nmiss(!vlist)=0.
-matrix.
-get clu /var= !clu.
-do if cmin(clu)=cmax(clu).
- comp aic= -999.
- comp bic= -999.
-else.
- comp clu= design(clu).
- comp n= csum(clu).
- comp sloglik= 0.
- comp cloglik= 0.
- comp sm= 0.
- comp cm= 0.
  !if (!scavars<>!null) !then
-  get vars /vari= !scavars. 
-  comp nmx= n(make(1,ncol(vars),1),:).
-  comp sum= t(vars)*clu.
-  comp ss= t(vars&*vars)*clu.
-  comp vrnc= (ss-sum&*sum/nmx)/nmx.
-  comp vrnct= (t(cssq(vars))-t(csum(vars))&**2/nrow(clu))/(nrow(clu)-1).
-  comp vrnct= vrnct*make(1,ncol(clu),1).
-  comp sloglik= csum(ln(vrnc+vrnct)/2).
-  comp sm= 2*ncol(vars).
  !ifend
  !if (!catvars<>!null) !then
-  get catvars /vari= !catvars.
-  comp ncat= make(1,ncol(catvars),0).
-  loop i= 1 to ncol(catvars).
-   comp cat= design(catvars(:,i)).
-   comp ncat(i)= ncol(cat).
-   comp cat= t(cat)*clu/n(make(1,ncol(cat),1),:).
-   comp cloglik= cloglik-csum(cat&*ln(cat+(cat=0))).
-  end loop.
-  comp cm= rsum(ncat-1).
  !ifend
- comp loglik= -n&*(sloglik+cloglik).
- comp m= ncol(clu)*(sm+cm).
- comp aic= -2*rsum(loglik)+2*m.
- comp bic= -2*rsum(loglik)+m*ln(nrow(clu)).
-end if.
-save {!quote(!clu),aic,bic} /out= !quote(!conc('temp_KO_aicbic_',!clu,'.sav')) /vari= clusol aic bic /strings= clusol.
-end matrix.
!doend
set results list.
add files /file= !quote(!conc('temp_KO_aicbic_',!head(!clusol),'.sav')). 
!do !clu !in (!tail(!clusol))
-add files /file= * /file= !quote(!conc('temp_KO_aicbic_',!clu,'.sav')).
!doend
recode aic bic (-999=sysmis).
cache.
exec.
var lab aic 'Akaike Information Criterion' bic 'Bayesian Information Criterion' clusol 'Cluster solution'.
var lev aic bic (sca).
format aic bic (f8.3).
list clusol aic bic.
!if (!tail(!clusol)<>!null) !then
-GRAPH /LINE(SIMPLE)= VALUE(aic) BY clusol /TITLE= 'Comparison of cluster solutions (AIC)' /FOOTNOTE= 'The lower the value the "better" is solution'.
-GRAPH /LINE(SIMPLE)= VALUE(bic) BY clusol /TITLE= 'Comparison of cluster solutions (BIC)' /FOOTNOTE= 'The lower the value the "better" is solution'.
!ifend
!do !clu !in (!clusol)
-erase file !quote(!conc('temp_KO_aicbic_',!clu,'.sav')).
!doend
!enddefine.
