import json
import csv
import sys
import os

file_fullname = sys.argv[1]
file_name = os.path.splitext(os.path.basename(file_fullname))[0]
# print('Reading ' + os.path.basename(file_fullname) + ' and creating ' + file_name + '.csv from it')

file_name += '_CONSTRUCTS'


with open(file_fullname) as user_file:
    user_csv = csv.reader(user_file)
    user_csv = list(user_csv)

with open(file_name + '.csv', 'w', newline='') as out_file:
    out_csv = csv.writer(out_file)

    num_failed_checks = 0
    failed_checks = []
    for i, row in enumerate(user_csv):

        # if (i == 0):
        #     for j, col in enumerate(row):
        #         print(str(j) + " - " + col)

        if (i == 0 or i == 2):
            continue
        if (i == 1):
            out_row = []
            # ResponseID 8
            out_row.append(row[8])
            out_row.append("Weapon")
            out_row.append("Walk State")
            out_row.append("Emotion")

            step = 12
            start = 28
            end = start + step
            for j in range(start, end):
                categories = [x.strip() for x in row[j].split('-')]
                text = categories[3].replace(":", ".")
                out_row.append(text)
            # print(', '.join(out_row))
            out_csv.writerow(out_row)

        else:
            # Attention checks
            attention_check_1 = row[100]
            attention_check_2 = row[172]
            attention_check_3 = row[252]

            attention_check_1_correct_str = 'Agree'
            attention_check_2_correct_str = 'Disagree'
            attention_check_3_correct_str = 'Somewhat agree'

            is_attention_check_1_correct = attention_check_1 == attention_check_1_correct_str
            is_attention_check_2_correct = attention_check_2 == attention_check_2_correct_str
            is_attention_check_3_correct = attention_check_3 == attention_check_3_correct_str

            # check if two of the three attention checks are wrong
            if (not is_attention_check_1_correct and not is_attention_check_2_correct) \
                    or (not is_attention_check_1_correct and  not is_attention_check_3_correct) \
                    or (not is_attention_check_2_correct and not is_attention_check_3_correct):
                print("Attention check failed for:" + row[8] + '/' + row[270])
                num_failed_checks += 1
                failed_checks.append(row[8] + '/' + row[270])
                continue

            durantion = row[5]
            min_durantion = 60 * 8 # 8 minutes
            if (int(durantion) < min_durantion):
                print("Duration check failed for:" + row[8] + '/' + row[270])
                num_failed_checks += 1
                failed_checks.append(row[8] + '/' + row[270])
                continue

            step = 12
            step_next = step + 3
            start = 28
            end = 0
            while True:
                end = start + step
                if (end > len(row) - 1):
                    break

                out_row = []
                # ResponseID 8
                out_row.append(row[8])

                categories = [x.strip()
                              for x in list(user_csv)[1][start].split('-')]
                states = [x.strip()
                          for x in categories[2].split(".")[0].split()]
                # Weapon
                out_row.append(states[1])
                # Walk State
                out_row.append(states[2])
                # Emotion
                out_row.append(states[3])

                for j in range(start, end):
                    out_row.append(row[j])
                start += step_next
                # print(', '.join(out_row))
                out_csv.writerow(out_row)
            
    print("Number of failed attention checks: " + str(num_failed_checks))
    print("Number of accepted cases: " + str(len(user_csv) - 2 - num_failed_checks))

# print failed_checks in new lines
# for failed_check in failed_checks:
#      print(failed_check)

# sys.stdout.write(file_name + '.csv')
