# JATOS JSON in R

This is a place to store and interpret data from JATOS and jspsych. I will use R to interpret the data.

JATOS exports the JSON output as a `.txt` file with each JSON entry in one line. To be able to interpret such a file, I create a small script named `fix-JATOS-json.sh` that picks an input `.txt` file and creates a `.json` file with each entry belonging to a big array. To use it run the following command:

```bash 
./fix-JATOS-json.sh jatos_results_XXX.txt
# this will output a jatos_results_XXX.json
# with the correctly formated JSON.
```