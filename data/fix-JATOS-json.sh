#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

FILE=$1
if [[ ! "$FILE" = /* ]]; then
  FILE="${DIR}/${FILE}"
fi

OLD_STRING=$']\n['
NEW_STRING=$'],\n['

FILE_CONTENT="$(<"${FILE}")"
FILE_NAME="$(basename "$FILE" | rev | cut -f 2- -d '.' | rev)"

# echo "Fixing JATOS JSON file ${FILE} into ${FILE_NAME}.json"

OUTJSON=${FILE_CONTENT//${OLD_STRING}/${NEW_STRING}}
BEGIN_STRING=$'[\n'
END_STRING=$'\n]'
OUTJSON="${BEGIN_STRING}${OUTJSON}${END_STRING}"
echo "${OUTJSON}" > "${FILE_NAME}.json"
echo "${FILE_NAME}.json"