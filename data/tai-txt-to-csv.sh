#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

FILE=$1

echo "Converting ${FILE} to correct JSON format..."
JSON_FILE=$(${DIR}/fix-JATOS-json.sh ${FILE})
echo "Created ${JSON_FILE}."
echo "Converting ${JSON_FILE} into CSV..."
CSV_FILE=$(python3 tai-json-to-csv.py "${DIR}/${JSON_FILE}")
CSV_DEMOGRAPHICS_FILE=$(python3 tai-json-to-csv_demographic.py "${DIR}/${JSON_FILE}")
echo "Created ${CSV_FILE} and ${CSV_DEMOGRAPHICS_FILE}."