import json
import csv
import sys
import os

file_fullname = sys.argv[1]
file_name = os.path.splitext(os.path.basename(file_fullname))[0]
# print('Reading ' + os.path.basename(file_fullname) + ' and creating ' + file_name + '.csv from it')

with open(file_fullname) as user_file:
    parsed_json = json.load(user_file)

# print(parsed_json)

# Prints a CSV File ready for a small content analysis

file_name += '_trials'

with open(file_name + '.csv', 'w', newline='') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter='$',
                            quotechar='"', quoting=csv.QUOTE_MINIMAL)
    spamwriter.writerow(['ID', 'PROLIFIC_PID', 'index', 'Alike Anim 01', 'Alike Anim 02',
                        'Unalike Anim', 'Alike Pole', 'Unalike Pole', 'Explanation'])
    for trial in parsed_json:
        # between index 5 and 8
        for i in range(5, 9):
            ID = trial[i]['WORKER_ID']
            PROLIFIC_PID = ""
            if "PROLIFIC_PID" in trial[i]:
                PROLIFIC_PID = trial[i]['PROLIFIC_PID']
            INDEX = trial[i]['trial_index'] - 4

            ALIKE_ANIMS = []
            for anim in trial[i]['response']['Alike Animations']:
                if anim == 'A':
                    ALIKE_ANIMS.append(trial[i]['combination'][0])
                if anim == 'B':
                    ALIKE_ANIMS.append(trial[i]['combination'][1])
                if anim == 'C':
                    ALIKE_ANIMS.append(trial[i]['combination'][2])
            ALIKE_ANIMS = sorted(ALIKE_ANIMS)

            if 'A' not in trial[i]['response']['Alike Animations']:
                UNALIKE_ANIM = trial[i]['combination'][0]
            elif 'B' not in trial[i]['response']['Alike Animations']:
                UNALIKE_ANIM = trial[i]['combination'][1]
            elif 'C' not in trial[i]['response']['Alike Animations']:
                UNALIKE_ANIM = trial[i]['combination'][2]

            POLE_1 = trial[i]['response']['Pole 1']
            POLE_2 = trial[i]['response']['Pole 2']
            EXPLANATION = trial[i]['response']['Explanation']
            row = [ID, PROLIFIC_PID, INDEX, ALIKE_ANIMS[0], ALIKE_ANIMS[1],
                   UNALIKE_ANIM, POLE_1, POLE_2, EXPLANATION]
            # print(row)
            spamwriter.writerow(row)

sys.stdout.write(file_name + '.csv')
