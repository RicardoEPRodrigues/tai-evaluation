import json
import csv
import sys
import os

file_fullname = sys.argv[1]
file_name = os.path.splitext(os.path.basename(file_fullname))[0]
# print('Reading ' + os.path.basename(file_fullname) + ' and creating ' + file_name + '.csv from it')

with open(file_fullname) as user_file:
    parsed_json = json.load(user_file)

# print(parsed_json)

# Prints a CSV File ready for a small content analysis

file_name += '_demographics'

with open(file_name + '.csv', 'w', newline='') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter='$',
                            quotechar='"', quoting=csv.QUOTE_MINIMAL)
    spamwriter.writerow(['ID', 'PROLIFIC_PID', 'Time Elapsed', 'Age',
                         'Gender', 'Maternal Language', 'Interacted with NPCs in videogames',
                        'Interacted with Virtual Assistants', 'Interacted with Social Robots',
                         'Worked with or developed Synthetic Characters'])
    for trial in parsed_json:
        # between index 5 and 8
        i = 1

        ID = trial[i]['WORKER_ID']
        PROLIFIC_PID = ""
        if "PROLIFIC_PID" in trial[i]:
            PROLIFIC_PID = trial[i]['PROLIFIC_PID']

        AGE = trial[i]['response']['Age']
        GENDER = trial[i]['response']['Gender']
        MATERNAL_LANG = trial[i]['response']['Maternal Language']

        options = ['Interacted with NPCs (non-player characters) in videogames',
                   'Interacted with Virtual Assistants (e.g. Siri, Cortana, Google Assistant, in museums, etc.)',
                   'Interacted with Social Robots (in scientific experiments, museums, etc.)',
                   'Worked with or developed Synthetic Characters/Virtual Characters']
        answers = [False, False, False, False]
        for j in range(0, 4):
            option = options[j]
            if option in trial[i]['response']['Interaction']:
                answers[j] = True

        i = 9
        TIME_ELAPSED = trial[i]['time_elapsed']

        row = [ID, PROLIFIC_PID, TIME_ELAPSED, AGE, GENDER,
               MATERNAL_LANG, answers[0], answers[1], answers[2], answers[3]]
        # print(row)
        spamwriter.writerow(row)

sys.stdout.write(file_name + '.csv')
