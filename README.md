# Adfectus TAI Method Evaluation

This project holds the questionnaire created to evaluate the Adfectus characters using the TAI method.

To run this questionnaire you need a JATOS server that will gather the data.

## JATOS JSON Input

You can customize some parameters in the questionnaire using JSON Input in the Study Properties of JATOS. Here follows a list of elements you can edit.

### duration_in_minutes

You can specify the duration of the study in minutes by editing the property `duration_in_minutes`. This changes the introductory text to display your custom number of minutes. Default is `15`.

```json
{
  // ...
  "duration_in_minutes": 15,
  // ...
}
```

### n_combinations_to_evaluate

You can specify the number of triads to show the participants by editing the property `n_combinations_to_evaluate`. Default is `4`.

```json
{
  // ...
  "n_combinations_to_evaluate": 4,
  // ...
}
```

### attention_check_index

Defines when in the triad timeline the attention checks will appear. Must be an array with integers between `1` and `n_combinations_to_evaluate`. Default is `[2, 4]`. Set to `[]` to disable the attention check.

```json
{
  // ...
  "attention_check_index": [2, 4],
  // ...
}
```

### contact_email

You can specify the email of the experiment's spokesperson by editing the property `contact_email`. Default is `ricardo.proenca.rodrigues@tecnico.ulisboa.pt`.

```json
{
  // ...
  "contact_email": "ricardo.proenca.rodrigues@tecnico.ulisboa.pt",
  // ...
}
```

### prolific_on_success_url and prolific_on_failure_url

See the section "Working with Prolific" below to know more about these variables.

### Usage Example

As an example, in JATOS in the `Study Properties`, add the following `JSON input`:

```json
{
  "prolific_on_success_url": "https://app.prolific.co/submissions/complete?cc=CZ947ZW1",
  "prolific_on_failure_url": "https://app.prolific.co/submissions/complete?cc=CZ1WPD1G",
  "duration_in_minutes": 15,
  "n_combinations_to_evaluate": 4,
  "attention_check_index": 2,
  "contact_email": "ricardo.proenca.rodrigues@tecnico.ulisboa.pt"
}
```

## Working with Prolific

This questionnaire is designed to be run on Prolific, to that end we have added a check to determine where the questionnaire is being runned from. If you pass the PROLIFIC_PID property in the URL, the software will assume it is running inside prolific and act accordingly.

Two variables need be set in JATOS to have the correct behavior, so in the `Study Properties`, add the following `JSON input`:

```json
{
  "prolific_on_success_url": "https://app.prolific.co/submissions/complete?cc=CZ947ZW1",
  "prolific_on_failure_url": "https://app.prolific.co/submissions/complete?cc=CZ1WPD1G"
}
```

`prolific_on_success_url` will be called when the test runs successfully. `prolific_on_failure_url` will be called when the participant fails the attention check exercise.


---

<details><summary>WIP Sections</summary>

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

</details>