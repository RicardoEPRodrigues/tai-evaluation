var jsPluginSurveyTextMultiSelect = (function (jspsych) {
  "use strict";

  const info = {
    name: 'survey-text-multi-select',
    description: '',
    parameters: {
      questions: {
        type: jspsych.ParameterType.COMPLEX,
        array: true,
        pretty_name: 'Questions Text',
        default: undefined,
        nested: {
          prompt: {
            type: jspsych.ParameterType.STRING,
            pretty_name: 'Prompt',
            default: undefined,
            description: 'Prompt for the subject to response'
          },
          number: {//check if it's a number, just for survey text
            type: jspsych.ParameterType.BOOL,
            pretty_name: 'Number',
            default: false,
            description: 'Check if it\'s a number, just for survey text'
          },
          options: {//just for multiselect
            type: jspsych.ParameterType.STRING,
            pretty_name: 'Options',
            array: true,
            default: undefined,
            description: 'Displays options for an individual question.'
          },
          horizontal: {//just for multiselect
            type: jspsych.ParameterType.BOOL,
            pretty_name: 'Horizontal',
            default: false,
            description: 'If true, then questions are centered and options are displayed horizontally.'
          },
          placeholder: {
            type: jspsych.ParameterType.STRING,
            pretty_name: 'Placeholder',
            default: "",
            description: 'Placeholder text in the textfield.'
          },
          rows: {
            type: jspsych.ParameterType.INT,
            pretty_name: 'Rows',
            default: 1,
            description: 'The number of rows for the response text box.'
          },
          columns: {
            type: jspsych.ParameterType.INT,
            pretty_name: 'Columns',
            default: 40,
            description: 'The number of columns for the response text box.'
          },
          required: {
            type: jspsych.ParameterType.BOOL,
            pretty_name: 'Required',
            default: false,
            description: 'Require a response'
          },
          value: {
            type: jspsych.ParameterType.STRING,
            pretty_name: 'Value',
            array: true,
            default: [''],
            description: 'if we want to put a default value'
          },
          name: {
            type: jspsych.ParameterType.STRING,
            pretty_name: 'Question Name',
            default: '',
            description: 'Controls the name of data values associated with this question'
          },
          question_type: {
            type: jspsych.ParameterType.STRING,
            pretty_name: 'Question Type',
            default: 'Text',
            description: 'The type of question, multiple choice, multiple select, multiple select pair or text'
          },
          disabled: {
            type: jspsych.ParameterType.BOOL,
            pretty_name: 'Disabled',
            default: false,
            description: 'Disable the possibility to answer'
          }
        }
      },
      preamble: {
        type: jspsych.ParameterType.STRING,
        pretty_name: 'Preamble',
        default: null,
        description: 'HTML formatted string to display at the top of the page above all the questions.'
      },
      button_label: {
        type: jspsych.ParameterType.STRING,
        pretty_name: 'Button label',
        default: 'Continue',
        description: 'The text that appears on the button to finish the trial.'
      },
      required_message: { //just for multiselect
        type: jspsych.ParameterType.STRING,
        pretty_name: 'Required message',
        default: 'You must choose at least one response for this question',
        description: 'Message that will be displayed if required question is not answered.'
      },
      pair_required_message: { //just for multiselect with pair
        type: jspsych.ParameterType.STRING,
        pretty_name: 'Required message',
        default: 'You must choose exactly two responses for this question',
        description: 'Message that will be displayed if required question is not answered.'
      },
      autocomplete: {
        type: jspsych.ParameterType.BOOL,
        pretty_name: 'Allow autocomplete',
        default: false,
        description: "Setting this to true will enable browser auto-complete or auto-fill for the form."
      }
    }
  }

  /**
  * **PluginSurveyTextMultiSelect**
  *
  * SHORT PLUGIN DESCRIPTION
  *
  * @author Ricardo Rodrigues
  * @see {@link https://DOCUMENTATION_URL DOCUMENTATION LINK TEXT}
  */
  class PluginSurveyTextMultiSelect {
    constructor(jsPsych) {
      this.jsPsych = jsPsych;
    }
    trial(display_element, trial) {

      for (var i = 0; i < trial.questions.length; i++) {
        if (typeof trial.questions[i].rows == 'undefined') {
          trial.questions[i].rows = 1;
        }
      }
      for (var i = 0; i < trial.questions.length; i++) {
        if (typeof trial.questions[i].columns == 'undefined') {
          trial.questions[i].columns = 40;
        }
      }
      for (var i = 0; i < trial.questions.length; i++) {
        if (typeof trial.questions[i].value == 'undefined') {
          trial.questions[i].value = "";
        }
      }

      var html = '';

      // inject CSS for trial
      html += '<style id="jspsych-survey-multi-select-css">';
      html += ".jspsych-survey-multi-select-question { margin-top: 2em; margin-bottom: 2em; text-align: left; }" +
        ".jspsych-survey-multi-select-text span.required {color: darkred;}" +
        ".jspsych-survey-multi-select-horizontal .jspsych-survey-multi-select-text {  text-align: left;}" +
        ".jspsych-survey-multi-select-option { line-height: 2; }" +
        ".jspsych-survey-multi-select-horizontal .jspsych-survey-multi-select-option {  display: inline-block;  margin-left: 1em;  margin-right: 1em;  vertical-align: top;}" +
        "label.jspsych-survey-multi-select-text input[type='checkbox'] {margin-right: 1em;}";
      html += '</style>';

      html += '<style id="jspsych-survey-multi-choice-css">';
      html += ".jspsych-survey-multi-choice-question { margin-top: 2em; margin-bottom: 2em; text-align: left; }" +
        ".jspsych-survey-multi-choice-text span.required {color: darkred;}" +
        ".jspsych-survey-multi-choice-horizontal .jspsych-survey-multi-choice-text {  text-align: center;}" +
        ".jspsych-survey-multi-choice-option { line-height: 2; }" +
        ".jspsych-survey-multi-choice-horizontal .jspsych-survey-multi-choice-option {  display: inline-block;  margin-left: 1em;  margin-right: 1em;  vertical-align: top;}" +
        "label.jspsych-survey-multi-choice-text input[type='radio'] {margin-right: 1em;}";
      html += '</style>';



      // show preamble text
      if (trial.preamble !== null) {
        html += '<div id="jspsych-survey-text-multi-select-preamble" class="jspsych-survey-text-multi-select-preamble">' + trial.preamble + '</div>';
      }
      // start form
      if (trial.autocomplete) {
        html += '<form id="jspsych-survey-text-multi-select-form">';
      } else {
        html += '<form id="jspsych-survey-text-multi-select-form" autocomplete="off">';
      }
      // generate question order
      var question_order = [];
      for (var i = 0; i < trial.questions.length; i++) {
        question_order.push(i);
      }
      if (trial.randomize_question_order) {
        question_order = jsPsych.randomization.shuffle(question_order);
      }

      // add questions
      for (var i = 0; i < trial.questions.length; i++) {
        var question = trial.questions[question_order[i]];
        var question_index = question_order[i];

        if (question.question_type === 'Text') {
          html += '<div id="jspsych-survey-text-' + question_index + '" class="jspsych-survey-text-question" style="margin: 2em 0em;text-align: left;"">';
          html += '<p class="jspsych-survey-text-">' + question.prompt;
          if (question.required) {
            html += "<span class='required' style='color:darkred;'>*</span>";
          }
          html += '</p>';
          var autofocus = i == 0 ? "autofocus" : "";
          var req = question.required ? "required" : "";
          if (question.disabled) {
            if (question.rows == 1) {
              html += '<input type="text" value="' + question.value[0] + '" id="input-' + question_index + '"  name="#jspsych-survey-text-response-' + question_index + '" data-name="' + question.name + '" size="' + question.columns + '" ' + autofocus + ' ' + req + ' placeholder="' + question.placeholder + '" disabled></input>';
            } else {
              html += '<textarea style="font-size: 14px; font-family: \'Open Sans\', \'Arial\' , sans-serif;" id="input-' + question_index + '" name="#jspsych-survey-text-response-' + question_index + '" data-name="' + question.name + '" cols="' + question.columns + '" rows="' + question.rows + '" ' + autofocus + ' ' + req + ' placeholder="' + question.placeholder + '" disabled>' + question.value[0] + '</textarea>';
            }
          }
          else {
            if (question.rows == 1) {
              html += '<input type="text" value="' + question.value[0] + '" id="input-' + question_index + '"  name="#jspsych-survey-text-response-' + question_index + '" data-name="' + question.name + '" size="' + question.columns + '" ' + autofocus + ' ' + req + ' placeholder="' + question.placeholder + '"></input>';
            } else {
              html += '<textarea style="font-size: 14px; font-family: \'Open Sans\', \'Arial\' , sans-serif;" id="input-' + question_index + '" name="#jspsych-survey-text-response-' + question_index + '" data-name="' + question.name + '" cols="' + question.columns + '" rows="' + question.rows + '" ' + autofocus + ' ' + req + ' placeholder="' + question.placeholder + '">' + question.value[0] + '</textarea>';
            }
          }
          html += '</div>';

        }
        else if (question.question_type === 'Multiple Select' || question.question_type === 'Multiple Select Pair') {//multi-select
          // create question container
          var question_classes = ['jspsych-survey-multi-select-question'];
          if (question.horizontal) {
            question_classes.push('jspsych-survey-multi-select-horizontal');
          }

          html += '<div id="jspsych-survey-multi-select-' + question_index + '" class="' + question_classes.join(' ') + '"  data-name="' + question.name + '">';

          // add question text
          html += '<p class="jspsych-survey-multi-select-text survey-multi-select">' + question.prompt
          if (question.required) {
            html += "<span class='required'>*</span>";
          }
          html += '</p>';

          // create option radio buttons
          for (var j = 0; j < question.options.length; j++) {
            // add label and question text
            var option_id_name = "jspsych-survey-multi-select-option-" + question_index + "-" + j;
            var input_name = 'jspsych-survey-multi-select-response-' + question_index;
            var input_id = 'jspsych-survey-multi-select-response-' + question_index + '-' + j;
            var checked = false;
            if (question.options[j] === question.value[j]) checked = true;

            //var required_attr = question.required ? 'required' : '';

            // add radio button container
            html += '<div id="' + option_id_name + '" class="jspsych-survey-multi-select-option">';
            html += '<label class="jspsych-survey-multi-select-text" for="' + input_id + '">';
            if (question.disabled) {
              if (checked) html += '<input type="checkbox" name="' + input_name + '" id="' + input_id + '" value="' + question.options[j] + '" checked disabled></input>';
              else html += '<input type="checkbox" name="' + input_name + '" id="' + input_id + '" value="' + question.options[j] + '" disabled></input>';
            }
            else {
              if (checked) html += '<input type="checkbox" name="' + input_name + '" id="' + input_id + '" value="' + question.options[j] + '" checked ></input>';
              else html += '<input type="checkbox" name="' + input_name + '" id="' + input_id + '" value="' + question.options[j] + '" ></input>';
            }
            html += question.options[j] + '</label>';
            html += '</div>';
          }

          html += '</div>';
        }
        else {//multi-choice
          // create question container
          var question_classes = ['jspsych-survey-multi-choice-question'];
          if (question.horizontal) {
            question_classes.push('jspsych-survey-multi-choice-horizontal');
          }

          html += '<div id="jspsych-survey-multi-choice-' + question_index + '" class="' + question_classes.join(' ') + '"  data-name="' + question.name + '">';

          // add question text
          html += '<p class="jspsych-survey-multi-choice-text survey-multi-choice">' + question.prompt
          if (question.required) {
            html += "<span class='required'>*</span>";
          }
          html += '</p>';

          // create option radio buttons
          for (var j = 0; j < question.options.length; j++) {
            // add label and question text
            var option_id_name = "jspsych-survey-multi-choice-option-" + question_index + "-" + j;
            var input_name = 'jspsych-survey-multi-choice-response-' + question_index;
            var input_id = 'jspsych-survey-multi-choice-response-' + question_index + '-' + j;

            var required_attr = question.required ? 'required' : '';

            // add radio button container
            html += '<div id="' + option_id_name + '" class="jspsych-survey-multi-choice-option">';
            html += '<label class="jspsych-survey-multi-choice-text" for="' + input_id + '">';
            html += '<input type="radio" name="' + input_name + '" id="' + input_id + '" value="' + question.options[j] + '" ' + required_attr + '></input>';
            if (question.options[j] === 'Other') {
              html += '<input type="text" style="font-size:17px;" id="other-input-' + question_index + '"  name="#other-' + question_index + '" data-name="Other" size="10"  placeholder="Other"></input>';
            }
            else {
              html += question.options[j];
            }
            html += '</label>';
            html += '</div>';
          }

          html += '</div>';
        }
      }

      // add submit button
      html += '<div class="fail-message"></div>';
      html += '<input type="submit" id="jspsych-survey-text-multi-select-next" class="jspsych-btn jspsych-survey-text-multi-select" value="' + trial.button_label + '"></input>';

      html += '</form>'
      display_element.innerHTML = html;

      // backup in case autofocus doesn't work
      // display_element.querySelector('#input-'+question_order[0]).focus();

      // validation check on the data first for custom validation handling
      // then submit the form
      display_element.querySelector('#jspsych-survey-text-multi-select-next').addEventListener('click', function () {
        var trial_form = display_element.querySelector("#jspsych-survey-text-multi-select-form");
        for (var i = 0; i < trial.questions.length; i++) {
          if (trial.questions[i].question_type === 'Multiple Select') {
            //if(document.querySelector('#jspsych-survey-text-'+i) === null){
            if (trial.questions[i].required) {
              var matches = display_element.querySelectorAll('#jspsych-survey-multi-select-' + i + ' input:checked')
              if (matches.length === 0) {
                display_element.querySelector('#jspsych-survey-multi-select-' + i + ' input').setCustomValidity(trial.required_message);
              }
              else {
                display_element.querySelector('#jspsych-survey-multi-select-' + i + ' input').setCustomValidity('');
              }
            }
          }
          if (trial.questions[i].question_type === 'Multiple Select Pair') {
            //if(document.querySelector('#jspsych-survey-text-'+i) === null){
            if (trial.questions[i].required) {
              var matches = display_element.querySelectorAll('#jspsych-survey-multi-select-' + i + ' input:checked')
              if (matches.length !== 2) {
                display_element.querySelector('#jspsych-survey-multi-select-' + i + ' input').setCustomValidity(trial.pair_required_message);
              }
              else {
                display_element.querySelector('#jspsych-survey-multi-select-' + i + ' input').setCustomValidity('');
              }
            }
          }
          if (trial.questions[i].question_type === 'Text') {
            if (trial.questions[i].number) {
              var q_element = display_element.querySelector('#jspsych-survey-text-' + i).querySelector('textarea, input');
              var val = q_element.value;
              if (!val.match(/^[0-9]+$/)) {
                q_element.setCustomValidity('Must be a number');
              }
              else {
                q_element.setCustomValidity('');
              }
            }
          }
          if (trial.questions[i].question_type === 'Multiple Choice') {
            var match = display_element.querySelector('#jspsych-survey-multi-choice-' + i);
            if (match.querySelector("input[type=radio]:checked") !== null) {
              var val = match.querySelector("input[type=radio]:checked").value;
              var other = display_element.querySelector('#other-input-' + i);
              if (other !== null && val === 'Other') {
                if (other.value === '') {
                  //other.setCustomValidity('Missing');
                  other.setAttribute("required", "");
                }
              }
              else if (other !== null) other.removeAttribute("required");
            }
          }
        }
        trial_form.reportValidity();
      })

      display_element.querySelector('#jspsych-survey-text-multi-select-form').addEventListener('submit', function (e) {
        e.preventDefault();
        // measure response time
        var endTime = performance.now();
        var response_time = endTime - startTime;

        // create object to hold responses
        var question_data = {};

        for (var index = 0; index < trial.questions.length; index++) {
          var id = "Q" + index;
          if (trial.questions[index].question_type === 'Text') {
            var q_element = display_element.querySelector('#jspsych-survey-text-' + index).querySelector('textarea, input');
            var val = q_element.value;
            var name = q_element.attributes['data-name'].value;
            if (name == '') {
              name = id;
            }
          }
          else if (trial.questions[index].question_type === 'Multiple Select' || trial.questions[index].question_type === 'Multiple Select Pair') {
            var match = display_element.querySelector('#jspsych-survey-multi-select-' + index);
            var inputboxes = match.querySelectorAll("input[type=checkbox]:checked");
            var val = [];
            for (var j = 0; j < inputboxes.length; j++) {
              const currentChecked = inputboxes[j];
              val.push(currentChecked.value)
            }

            var name = id;
            if (match.attributes['data-name'].value !== '') {
              name = match.attributes['data-name'].value;
            }
          }
          else {//multiple choice
            var match = display_element.querySelector('#jspsych-survey-multi-choice-' + index);
            if (match.querySelector("input[type=radio]:checked") !== null) {
              var val = match.querySelector("input[type=radio]:checked").value;
              if (val === 'Other') {
                val = display_element.querySelector('#other-input-' + index).value;
              }
            } else {
              var val = "";
            }
            var name = id;
            if (match.attributes['data-name'].value !== '') {
              name = match.attributes['data-name'].value;
            }
          }

          var obje = {};
          obje[name] = val;
          Object.assign(question_data, obje);
        }
        // save data
        var trialdata = {
          rt: response_time,
          response: question_data
        };

        display_element.innerHTML = '';

        // next trial
        jsPsych.finishTrial(trialdata);
      });

      var startTime = performance.now();
    }
  }
  PluginSurveyTextMultiSelect.info = info;

  return PluginSurveyTextMultiSelect;
})(jsPsychModule);