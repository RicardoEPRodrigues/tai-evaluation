"use strict";
var jsPsych;

jatos.onLoad(() => {

    var triad_combinations = [];
    // Number of combinations of triads to evaluate
    var n_combinations_to_evaluate = 4;
    if (typeof jatos.studyJsonInput.n_combinations_to_evaluate !== 'undefined') {
        n_combinations_to_evaluate = Number(jatos.studyJsonInput.n_combinations_to_evaluate);
    }
    var n_trials = 7 + n_combinations_to_evaluate;
    var n_total_combinations = -1;

    // Duration of the experiment
    var duration_in_minutes = 15;
    if (typeof jatos.studyJsonInput.duration_in_minutes !== 'undefined') {
        duration_in_minutes = Number(jatos.studyJsonInput.duration_in_minutes);
    }

    // Email of the experiment spokesperson
    var contact_email = "ricardo.proenca.rodrigues@tecnico.ulisboa.pt";
    if (typeof jatos.studyJsonInput.contact_email !== 'undefined') {
        contact_email = String(jatos.studyJsonInput.contact_email);
    }

    // Defines when in the triad timeline the attention check will appear. Must be between `1` and `n_combinations_to_evaluate`. Set to `0` to disable the attention check.
    var attention_check_index = [2, 4];
    if (typeof jatos.studyJsonInput.attention_check_index !== 'undefined') {
        attention_check_index = jatos.studyJsonInput.attention_check_index;
    }
    var attention_checks = [
        {
            question: {
                prompt: "Please select 'Agree' to show you are paying attention to this question:",
                options: ["Strongly agree", "Agree", "Disagree", "Strongly disagree"],
                required: true,
                question_type: 'Multiple Choice',
                name: 'Attention Check'
            },
            answer: 'Agree'
        },
        {
            question: {
                prompt: "Please select 'Disagree' to show you are paying attention to this question:",
                options: ["Strongly agree", "Agree", "Disagree", "Strongly disagree"],
                required: true,
                question_type: 'Multiple Choice',
                name: 'Attention Check'
            },
            answer: 'Disagree'
        },
        {
            question: {
                prompt: "Please select 'Strongly disagree' to show you are paying attention to this question:",
                options: ["Strongly agree", "Agree", "Disagree", "Strongly disagree"],
                required: true,
                question_type: 'Multiple Choice',
                name: 'Attention Check'
            },
            answer: 'Strongly disagree'
        },
        {
            question: {
                prompt: "Please select 'Strongly agree' to show you are paying attention to this question:",
                options: ["Strongly disagree", "Disagree", "Agree", "Strongly agree"],
                required: true,
                question_type: 'Multiple Choice',
                name: 'Attention Check'
            },
            answer: 'Strongly agree'
        }
    ]

    var in_prolific = false;

    jsPsych = initJsPsych({
        use_webaudio: false,
        show_progress_bar: true,
        auto_update_progress_bar: false,
        on_finish: function () {

            // jsPsych.data.displayData('json');

            jsPsych.getDisplayElement().innerHTML = '<p style="text-align: justify; padding-left: 8em; padding-right: 8em;"> \
                    Thank you for your participation in this study. All data was received.<br>\
                    For any further questions you can send an email to <a href=mailto:' + contact_email + '?subject="Expression Evaluation">' + contact_email + '</a>.</p>'

            if (in_prolific && typeof jatos.studyJsonInput.prolific_on_success_url !== 'undefined') {
                var URL = jatos.studyJsonInput.prolific_on_success_url;
                jatos.endStudyAndRedirect(URL, jsPsych.data.get().json());
            } else {
                jatos.endStudy(jsPsych.data.get().json());
            }
        }
    });

    function setup() {
        const weapon_options = ['axe', 'sword'];
        const move_status_options = ['idle', 'walk'];
        // var emotion_options = ['neutral', 'rage', 'confident', 'fear', 'defensive'];
        const emotion_options = ['rage', 'confident', 'fear', 'defensive'];

        /**
         * Returns all possible combinations of three animations.
         */
        function AdfectusAllCombinations() {
            console.log("AdfectusAllCombinations");

            var adfectus_animations = [];
            for (let weapon_index = 0; weapon_index < weapon_options.length; weapon_index++) {
                const weapon = weapon_options[weapon_index];
                for (let move_status_index = 0; move_status_index < move_status_options.length; move_status_index++) {
                    const move_status = move_status_options[move_status_index];
                    for (let emotion_index = 0; emotion_index < emotion_options.length; emotion_index++) {
                        const emotion = emotion_options[emotion_index];
                        adfectus_animations.push("TAI- " + weapon + " " + move_status + " " + emotion + ".mp4");
                    }
                }
            }
            console.log(adfectus_animations);
            var adfectus_animations_combinations = [];
            // Create combinations of triplets
            for (let j = 0; j < adfectus_animations.length - 2; j++) {
                for (let k = j + 1; k < adfectus_animations.length - 1; k++) {
                    for (let y = k + 1; y < adfectus_animations.length; y++) {
                        adfectus_animations_combinations.push([adfectus_animations[j], adfectus_animations[k], adfectus_animations[y]]);
                    }
                }
            }
            console.log(adfectus_animations_combinations);

            return adfectus_animations_combinations;
        }

        /**
         * Returns a shorter and simplified combination of animations focusing on emotion expression.
         * Two sets are created: 1) all combination of three emotions (4 combinations); 2) all combinations of weapon and move states (4 combinations)
         * The two sets are then combined. The idea is to present the same weapon/move state with different emotions.
         */
        function AdfectusCombinationsAnimVsEmotion() {

            var emotions_combinations = [];
            for (let j = 0; j < emotion_options.length - 2; j++) {
                for (let k = j + 1; k < emotion_options.length - 1; k++) {
                    for (let y = k + 1; y < emotion_options.length; y++) {
                        emotions_combinations.push([emotion_options[j], emotion_options[k], emotion_options[y]]);
                    }
                }
            }
            // console.log(emotions_combinations);

            var weapon_move_combinations = [];
            for (let weapon_index = 0; weapon_index < weapon_options.length; weapon_index++) {
                const weapon = weapon_options[weapon_index];
                for (let move_status_index = 0; move_status_index < move_status_options.length; move_status_index++) {
                    const move_status = move_status_options[move_status_index];
                    weapon_move_combinations.push([weapon, move_status]);
                }
            }
            // console.log(weapon_move_combinations);

            var animations_combinations = [];
            for (let weapon_move_index = 0; weapon_move_index < weapon_move_combinations.length; weapon_move_index++) {
                const weapon_move = weapon_move_combinations[weapon_move_index];
                const emotions_per_weapon_move = []
                for (let emotion_index = 0; emotion_index < emotions_combinations.length; emotion_index++) {
                    const emotion_triple = emotions_combinations[emotion_index];
                    const emotion_anims = [];
                    for (let i = 0; i < emotion_triple.length; i++) {
                        const emotion = emotion_triple[i];
                        emotion_anims.push("TAI- " + weapon_move[0] + " " + weapon_move[1] + " " + emotion + ".mp4");
                    }
                    emotions_per_weapon_move.push(emotion_anims);
                }
                animations_combinations.push(emotions_per_weapon_move);
            }
            // console.log(animations_combinations);

            return animations_combinations;
        }

        /**
         * Adds the combinations with an offset based on the number of already performed experiments.
         * Assumes the `combinations` variable is an array of triplets. e.g. combinations[0] = [anim1, anim2, anim3]
         */
        function LinearDisplayOfCombinations(possible_combinations) {
            var count = 0;
            for (var i = n_experiments; i < n_experiments + n_combinations_to_evaluate; i++) {
                var combination = possible_combinations[i % possible_combinations.length];
                var x = { v1: jsPsych.randomization.shuffle(combination), v2: ++count };
                triad_combinations.push(x);
            }
            n_total_combinations = count;
        }

        /**
         * Adds the combinations if they are split between weapon/move and emotion with an offset on the number of already performed experiments.
         * Assumes the `combinations` variable is a matrix of arrays (composed through arrays), the final arrays contain the animations of emotions. 
         * e.g. combinations[0] = [ [ [axe_anim1, axe_anim2, axe_anim3], [axe_anim1, axe_anim3, axe_anim4] ...], [ [sword_anim1, sword_anim2, sword_anim3], ...], ...]
         */
        function AnimVsEmotionCombinations(possible_combinations) {

            var emotion_index = n_experiments;

            var final_combinations = [];

            while (emotion_index < n_experiments + n_combinations_to_evaluate) {
                for (let i = 0; i < possible_combinations.length; i++) {
                    const weapon_move_combinations = possible_combinations[i];

                    var index = emotion_index++ % weapon_move_combinations.length;
                    var combination = weapon_move_combinations[index];
                    final_combinations.push(combination);

                    if (emotion_index >= n_experiments + n_combinations_to_evaluate) {
                        break;
                    }
                }
            }

            var count = 0;
            const shuffled_combinations = jsPsych.randomization.shuffle(final_combinations);
            for (let i = 0; i < shuffled_combinations.length; i++) {
                const combination = shuffled_combinations[i];
                var x = { v1: jsPsych.randomization.shuffle(combination), v2: ++count };
                triad_combinations.push(x);
            }

            n_total_combinations = count;
        }

        // console.log(n_experiments);
        jsPsych.data.addProperties({ WORKER_ID: jatos.workerId });

        if (typeof jatos.urlQueryParameters.PROLIFIC_PID !== 'undefined') {
            in_prolific = true;
            jsPsych.data.addProperties({ PROLIFIC_PID: jatos.urlQueryParameters.PROLIFIC_PID });
            jsPsych.data.addProperties({ STUDY_ID: jatos.urlQueryParameters.STUDY_ID });
            jsPsych.data.addProperties({ SESSION_ID: jatos.urlQueryParameters.SESSION_ID });
            console.log("In Prolific!");
        }

        // number of experiments already ran. It is used to shift the combinations shown to the participant.
        var n_experiments = Number(jatos.workerId);

        // combinations = AdfectusAllCombinations();
        var possible_combinations = AdfectusCombinationsAnimVsEmotion();

        // LinearDisplayOfCombinations();
        AnimVsEmotionCombinations(possible_combinations);

        console.log(triad_combinations);
    }

    var intro = {
        type: jsPsychHtmlButtonResponse,
        stimulus: function () {
            return '<h1>Expressions on Synthetic Characters</h1>\
                    <div class="description">\
                   <p> Thank you for your participation in this study. This study is  part of a Ph.D. dissertation at Instituto Superior Técnico that aims to <b>study the animation of expressions in synthetic characters</b>. \
                    Your participation and collaboration are, therefore, very much appreciated. <br><br>\
                    The experiment consists in watching very short videos (a few seconds each) depicting different expressions and comparing these animations. \
                     You are free to rewatch the videos as many times as you so desire.\
                     The experiment should take about <b>' + String(duration_in_minutes) + ' minutes</b>. All the answers will be anonymous and used solely for statistical purposes.<br></p>\
                    <p>We also gently remind you that:<br>\
                    - participation is voluntary and you can withdraw at any time;<br>\
                    - you have the right to ask any question related to the experiment at any given time (e-mail: <a href=mailto:' + contact_email + '?subject="Expression Evaluation">' + contact_email + '</a>);<br>\
                    - you will not be identified at any stage of the study and individual results will not be shared;<br>\
                    - your participation does not involve physical or psychological risks;<br>\
                    - at some point you will be asked to complete one or more attention check tasks, failure in doing so will terminate the experiment in a fail state.<br>\
                    By proceeding to the questionnaire you are giving your consent.<br><br>\
                    Thank you for your time and consideration.</p>\
                    </div>';
        },
        choices: ['Continue'],
        on_finish: function () {
            // at the end of each trial, update the progress bar
            // based on the current value and the proportion to update for each trial
            var curr_progress_bar_value = jsPsych.getProgressBarCompleted();
            jsPsych.setProgressBar(curr_progress_bar_value + (1 / n_trials));
        }
    }

    var demographic = {
        type: jsPluginSurveyTextMultiSelect,
        questions: [
            { prompt: 'Age:', options: [], columns: 10, name: 'Age', number: true, required: true },
            { prompt: 'Gender:', options: ['Female', 'Male', 'Prefer not to say', 'Other'], name: 'Gender', question_type: 'Multiple Choice', required: true },
            { prompt: 'Maternal Language:', options: [], name: 'Maternal Language', columns: "30", required: true },
            {
                prompt: 'What is your experience with Synthetic Characters?',
                options: ['Interacted with NPCs (non-player characters) in videogames',
                    'Interacted with Virtual Assistants (e.g. Siri, Cortana, Google Assistant, in museums, etc.)',
                    'Interacted with Social Robots (in scientific experiments, museums, etc.)',
                    'Worked with or developed Synthetic Characters/Virtual Characters'],
                name: 'Interaction', question_type: 'Multiple Select'
            }
        ],
        preamble: function () {
            return '';
        },
        data: { task: 'demographic' },
        on_finish: function () {
            // at the end of each trial, update the progress bar
            // based on the current value and the proportion to update for each trial
            var curr_progress_bar_value = jsPsych.getProgressBarCompleted();
            jsPsych.setProgressBar(curr_progress_bar_value + (1 / n_trials));
        }
    };

    var example = {
        type: jsPsychHtmlButtonResponse,
        stimulus: '<h1>The Experiment</h1>\
                    <div class="description">\
                   <p>In the following 6 sections you will be asked to compare 3 animations \
                    with different expressions by telling us the <b>two that are more alike</b>, <b>why</b> the two are more alike and how they <b>differ from the third</b>.<br><br>\
                    Here is an example:<br>Consider 3 different shapes, a <b>circle</b>, a <b>square</b> and a <b>triangle</b>.\
                    We could say for example that the <b>square</b> and the <b>triangle</b> are more <b>alike</b> because they are defined by <b>straight lines</b>\
                    and they are <b>different</b> from a <b>circle</b> because it has <b>curved lines</b>.</p> \
                    <p class="callout">Please, do not reuse the same pair of characteristics more than once, even if it is the most obvious characteristic to distinguish between the animations.</p>\
                    <p>The following section exemplifies how the questionnaire would be filled.</p>\
                    </div>',
        choices: ['Continue'],
        on_finish: function () {
            // at the end of each trial, update the progress bar
            // based on the current value and the proportion to update for each trial
            var curr_progress_bar_value = jsPsych.getProgressBarCompleted();
            jsPsych.setProgressBar(curr_progress_bar_value + (1 / n_trials));
        }
    }

    var triad_example = {
        type: jsPluginSurveyTextMultiSelect,
        questions: [
            {
                prompt: 'By comparing the 3 animations presented above, identify the two that are alike:',
                options: ['A', 'B', 'C'],
                horizontal: true,
                question_type: 'Multiple Select Pair',
                value: ['A', 'B'],
                disabled: true
            },
            {
                prompt: 'Considering your previous answer, how are two of them alike and at the same time different from the third:',
                options: [], columns: 130, rows: 3, disabled: true,
                value: ['A and B are more alike because they are defined by straight lines and they are different from C because it has curved lines']
            },
            {
                prompt: 'Provide us with one characteristic that you found was alike in the two shapes:<br><b>(Use 1 to 3 words)</b>',
                options: [], columns: 30, disabled: true,
                value: ['straight lines']
            },
            {
                prompt: 'Provide us with the opposite characteristic from the one mentioned above, describing the different shape:<br><b>(Use 1 to 3 words)</b>',
                options: [], columns: 30, disabled: true,
                value: ['curved lines']
            }

        ],
        preamble: function () {
            return '<h2>Example</h2>\
                    <p>Example of a possible answer.<br><br>The images below are representing three different shapes.<br><br></p>\
                    <div class="item"> \
                        <img src="imagens/square.png" alt="figures" style="text-align:center;">\
                        <span class="caption">A</span>\
                    </div>\
                    <div class="item"> \
                        <img src="imagens/triangle.png" alt="figures" style="text-align:center;">\
                        <span class="caption">B</span>\
                    </div>\
                    <div class="item"> \
                        <img src="imagens/circle.png" alt="figures" style="text-align:center;">\
                        <span class="caption">C</span>\
                    </div>';
        },
        on_finish: function () {
            // at the end of each trial, update the progress bar
            // based on the current value and the proportion to update for each trial
            var curr_progress_bar_value = jsPsych.getProgressBarCompleted();
            jsPsych.setProgressBar(curr_progress_bar_value + (1 / n_trials));
        }
    };

    var begin = {
        type: jsPsychHtmlButtonResponse,
        stimulus: '<h2>Beginning of the experiment</h2>\
                   <p>The experiment itself starts in the next section.<br><br>\
                    Below you can find the example that was given before, in PDF format:<br><br>\
                    <a href="example_Part1.pdf" download>Example</a><br>\
                    <p>',
        choices: ['Continue'],
        on_finish: function () {
            // at the end of each trial, update the progress bar
            // based on the current value and the proportion to update for each trial
            var curr_progress_bar_value = jsPsych.getProgressBarCompleted();
            jsPsych.setProgressBar(curr_progress_bar_value + (1 / n_trials));
        }
    }

    var triad = {
        type: jsPluginSurveyTextMultiSelect,
        questions: function () {
            var qs = [
                {
                    prompt: 'By comparing the 3 animations presented above, identify the two that are alike:',
                    options: ['A', 'B', 'C'],
                    name: 'Alike Animations',
                    horizontal: true,
                    question_type: 'Multiple Select Pair',
                    required: true
                },
                {
                    prompt: 'Considering your previous answer, how are two of them alike and at the same time different from the third:',
                    options: [],
                    columns: 130,
                    rows: 3,
                    name: 'Explanation',
                    required: true
                },
                {
                    prompt: function () {
                        var txt = '';
                        if (jsPsych.timelineVariable('v2') !== 1) {
                            txt = '<p class="callout"><i>Please <b>do not repeat</b> characteristics from previous sections</i><p>';
                        }
                        return txt + 'Provide us with one characteristic that you found was alike in the two animations:<br><b>(Use 1 to 3 words)</b>'
                    }(),
                    options: [], columns: 30, name: 'Pole 1', required: true
                },
                {
                    prompt: 'Provide us with the opposite characteristic from the one mentioned above, describing the different animation:<br><b>(Use 1 to 3 words)</b>',
                    options: [],
                    columns: 30,
                    name: 'Pole 2',
                    required: true
                }

            ];
            attention_check_index.forEach((attention_index, list_index) => {
                if (jsPsych.timelineVariable('v2') === attention_index) {
                    qs.push(attention_checks[list_index % attention_checks.length].question);
                }
            });
            return qs;
        },
        preamble: function () {
            return '<h2>Comparison ' + jsPsych.timelineVariable('v2') + ' of ' + n_total_combinations + '</h2>\
                    <p>The videos below are showing 3 animations of different emotion expressions.<br><br></p>\
                    <div class="item"> \
                        <video width="300" controls> <source src="videos/'+ jsPsych.timelineVariable('v1')[0] + '" type="video/mp4"> </video>  \
                        <span class="caption">A</span>\
                    </div>\
                    <div class="item"> \
                        <video width="300" controls> <source src="videos/'+ jsPsych.timelineVariable('v1')[1] + '" type="video/mp4"> </video>  \
                        <span class="caption">B</span>\
                    </div>\
                    <div class="item"> \
                        <video width="300" controls> <source src="videos/'+ jsPsych.timelineVariable('v1')[2] + '" type="video/mp4"> </video>  \
                        <span class="caption">C</span>\
                    </div> \
                    <p class="callout"><i>In the following questions, please describe the observed behavior supporting your answers and not your interpretation of the behavior.\
                    </br>For instance, <b>do not use</b> references to emotions (e.g., anger) but refer to the underlying behavior supporting your interpretation (e.g., grumbling).</i></p>';
        },
        data: { task: 'triad', combination: jsPsych.timelineVariable('v1') },
        on_finish: function (data) {
            attention_check_index.forEach((attention_index, list_index) => {
                if (jsPsych.timelineVariable('v2') === attention_index &&
                    data.response["Attention Check"] !== attention_checks[list_index % attention_checks.length].answer) {
                    var fail_message = "Failed attention check #" + String(list_index + 1);
                    if (in_prolific && typeof jatos.studyJsonInput.prolific_on_failure_url !== 'undefined') {
                        var URL = jatos.studyJsonInput.prolific_on_failure_url;
                        jatos.endStudyAndRedirect(URL, false, fail_message);
                    } else {
                        jatos.endStudy(false, fail_message);
                    }
                }
            });
            // at the end of each trial, update the progress bar
            // based on the current value and the proportion to update for each trial
            var curr_progress_bar_value = jsPsych.getProgressBarCompleted();
            jsPsych.setProgressBar(curr_progress_bar_value + (1 / n_trials));
        }
    };

    var triad_procedure = {
        timeline: [triad],
        timeline_variables: triad_combinations
    }


    var submit = {
        type: jsPsychHtmlButtonResponse,
        stimulus: function() { return '<h1>End of the experiment</h1>\
                   <div class="description">\
                   <p>Thank you for your participation in this study. <b>Press the submit button below to finish your participation.</b></p>\
                   <p>I would like to remind you that all data is used solely for statistical purposes.</p> \
                   <p>For any further questions you can send an email to <a href=mailto:' + contact_email + '?subject="Expression Evaluation">' + contact_email + '</a>.</p>\
                    </div>';},
        choices: ['Submit'],
        on_finish: function () {
            // at the end of each trial, update the progress bar
            // based on the current value and the proportion to update for each trial
            var curr_progress_bar_value = jsPsych.getProgressBarCompleted();
            jsPsych.setProgressBar(curr_progress_bar_value + (1 / n_trials));
        }
    }

    jatos.addAbortButton();
    jatos.showBeforeUnloadWarning(true);
    setup();

    var timeline = [intro, demographic, example, triad_example, begin, triad_procedure, submit];
    // var timeline = [triad_procedure, submit];

    jsPsych.run(timeline);
});